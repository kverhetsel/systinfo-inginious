# DOCKER-VERSION 1.1.0

FROM ingi/inginious-c-default

RUN rpm -Uvh http://dl.fedoraproject.org/pub/epel/beta/7/x86_64/epel-release-7-0.2.noarch.rpm
RUN yum -y group install "Development Tools"
RUN yum -y install clang-devel check-devel valgrind

ADD . /systinfo-inginious
RUN cd systinfo-inginious && \
    autoreconf --install && \
    LDFLAGS=-L/usr/lib64/llvm ./configure --prefix=/usr && \
    make install

RUN  echo /usr/lib >> /etc/ld.so.conf && \
     echo /usr/lib64/llvm >> /etc/ld.so.conf && \
     ldconfig
