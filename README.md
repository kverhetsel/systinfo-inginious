This repository contains programming exercises in the C language for a class at
the Université Catholique de Louvain
([Systèmes informatiques](http://sites.uclouvain.be/SystInfo/)), as well as
tools to check them automatically. The exercises are meant to be run using the
[INGInious](https://github.com/UCL-INGI/INGInious) platform.

Directory structure
-------------------

- ``problems``: The list of problems available in INGInious, and code to check
  solutions to them.
- ``matchers``: A library built on top of ``libclang`` to check for certain patterns
  in C code, so as to give better feedback to the user.
- ``check_helpers``: A library with convenience functions to be used with Check
  for unit testing.
- ``stdlib_hooks``: A library to override library functions, in particular
  to force memory allocation to fail.
- ``scripts``: Some programs that are used to compile and run the student's
  code.

Dependencies
------------

- ``INGInious``
- ``libclang`` (including header files)
- [Check](http://check.sourceforge.net/) (including header files)
- [Automake](http://www.gnu.org/software/automake/)

Installation
------------

You first need to follow the steps to install INGInious. Once that is done, you
can build the Docker environment used to run student code:

    # The next line assumes this is the current working directory.
    sudo docker build -t systinfo .

You need to adjust INGInious's configuration so that it knows about this
environment. The ``configuration.json`` file should include something like this:

    "containers": {
    	"default": "ingi/inginious-c-default",
    	"sekexe": "ingi/inginious-c-sekexe",
        "systinfo": "systinfo"
    },

You also need to copy the ``problems`` sub-directory to INGInious's ``tasks``
directory.

     cp -r problems /path/to/INGInious/tasks/systinfo

Writing Tasks
-------------

See [task_guide.md](task_guide.md) for a guide on how to write tasks.

Compiling The Libraries On Your Own System
------------------------------------------

You can skip this section if you only want to use the libraries that are part of
this project on INGInious and not on your own system (which is most likely the
case).

The following sequence of commands can be used to compile and install all the
libraries:

    autoreconf --install
    ./configure
    make install

Regenerating graphs
-------------------

Some exercises benefit from having a graph summarize the task at hand. The
projet is configured so you can generate some using TikZ/PGF. If you wish to
change or regenerate them, you will need a working LaTeX and TikZ/PGF
installation as well as ImageMagick's ``convert`` tool. The following command
generates ``png`` images from ``tex`` files:

    make graphs

To add a new file, add it as a dependency to the ``graphs`` task in
``Makefile.am``.
