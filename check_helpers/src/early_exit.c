#include <semaphore.h>
#include <sys/mman.h>
#include <stdio.h>

static struct {
  sem_t start_sem;
  sem_t end_sem;
} *shared_data;

int check_early_exit_setup(void) {
  shared_data = mmap(NULL, sizeof(*shared_data), PROT_READ | PROT_WRITE,
                     MAP_ANONYMOUS | MAP_SHARED, -1, 0);
  if (!shared_data) return -1;

  if (sem_init(&shared_data->start_sem, 1, 0) != 0) {
    munmap(shared_data, sizeof(*shared_data));
    return -1;
  }

  if (sem_init(&shared_data->end_sem, 1, 0) != 0) {
    sem_destroy(&shared_data->start_sem);
    munmap(shared_data, sizeof(*shared_data));
    return -1;
  }

  return 0;
}

int check_early_exit(int number_failed) {
  if (number_failed == 0) {
    int number_started, number_finished;
    sem_getvalue(&shared_data->start_sem, &number_started);
    sem_getvalue(&shared_data->end_sem, &number_finished);

    if (number_started != number_finished) {
      fprintf(stderr, "Erreur: Certains des tests se sont terminés "
              "prématurément via exit.\n");
      number_failed++;
    }
  }

  sem_destroy(&shared_data->start_sem);
  sem_destroy(&shared_data->end_sem);
  munmap(shared_data, sizeof(*shared_data));

  return number_failed;
}

void check_log_test_started(const char *name);
void check_log_test_finished(void);

static
void finished(void) {
  sem_post(&shared_data->end_sem);
  check_log_test_finished();
}

void (*check_helpers_test_finished)(void) = &finished;

void check_helpers_test_started(const char *name) {
  sem_post(&shared_data->start_sem);
  check_log_test_started(name);
}
