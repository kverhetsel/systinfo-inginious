#include "check_helpers.h"
#include <semaphore.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdio.h>

static size_t test_count = 0;

typedef enum {
  Started, Done,
} child_status;

typedef struct child {
  pid_t pid;
  const char *name;
  char data[2048];
  child_status status;
} child;

struct {
  /* Tests aren't supposed to run in parallel but this shouldn't hurt */
  sem_t lock;

  size_t size, capacity;
  child children[];
} *shared_data = NULL;

static
child *find_current_child(void) {
  if (!shared_data) return NULL;

  child *ret = NULL;
  pid_t pid = getpid();

  sem_wait(&shared_data->lock); {
    size_t i;
    for (i = 0; i < shared_data->size; i++) {
      if (shared_data->children[i].pid == pid) break;
    }

    if (i == shared_data->size && i < shared_data->capacity) {
      shared_data->children[i].pid = pid;
      shared_data->children[i].data[0] = '\0';
      shared_data->children[i].status = Started;
      shared_data->size++;
    }

    if (i < shared_data->size) ret = &shared_data->children[i];
  } sem_post(&shared_data->lock);

  return ret;
}

void tcase_add_logging_test_f(TCase *tc, TFun tf,
                              const char *fname, int signal,
                              int allowed_exit_value, int start,
                              int end) {
  _tcase_add_test(tc, tf, fname, signal, allowed_exit_value, start, end);
  test_count++;
}

void check_log(const char *fmt, ...) {
  child *child = find_current_child();
  if (child) {
    va_list list;
    va_start(list, fmt);
    vsnprintf(child->data, sizeof(child->data), fmt, list);
    va_end(list);
  }
}

int check_init_logs(void) {
  shared_data = mmap(NULL, sizeof(*shared_data) + sizeof(child)*test_count,
                     PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, -1, 0);
  if (!shared_data) return -1;

  shared_data->size = 0;
  shared_data->capacity = test_count;

  if (sem_init(&shared_data->lock, 1, 1) != 0) {
    munmap(shared_data, sizeof(*shared_data)+sizeof(child)*test_count);
    return -1;
  }

  return 0;
}

void check_log_test_started(const char *name) {
  child *child = find_current_child();
  if (child) child->name = name;
}

void check_log_test_finished(void) {
  child *child = find_current_child();
  if (child) child->status = Done;
}

void check_print_logs(void) {
  if (!shared_data) return;

  for (size_t i = 0; i < shared_data->size; i++) {
    if (shared_data->children[i].status != Done &&
        shared_data->children[i].data[0]) {
      fprintf(stderr, "%s s’est terminé après le message suivant : %s\n",
              shared_data->children[i].name,
              shared_data->children[i].data);
    }
  }

  sem_destroy(&shared_data->lock);
  munmap(shared_data, sizeof(*shared_data)+sizeof(child)*shared_data->capacity);
}
