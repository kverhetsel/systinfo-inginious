Matchers
========

The matchers API allows to look for patterns in C source code. There are various
functions that return new matchers or combine existing ones, which can be used
to express relatively complex rules (see annotations in ``matchers.h`` for a
description of each of them).

For example this is how you'd express the rule "match the sum of a pointer and a
product between a number and an expression that uses ``sizeof``":

    matcher m = match_opseq(MatcherOpAdd,
                            match_pointer(),
                            match_opseq(MatcherOpMul,
                                        match_anything(),
                                        match_sizeof()));

Here are some examples of expressions that are matched by ``m`` (assuming ``p`` was
defined as a pointer or an array):

     p + 3*sizeof(int)
     3*sizeof(int) + p
     p + 1 + (2*sizeof(int))
     p + (condition ? ((int)4)*sizeof(int) : 0)

When you're done using a matcher, don't forget to release it:

    matcher_release(m);

Basic usage
-----------

To run a matcher, a handle to a file needs to be acquired. ``matcher_open`` can
be used to parse a file and acquire that handle:

    cont char *argv[3] = {"-Idir_1", "-Idir_2", MatcherClangIncludePath};
    matcher_file f = matcher_open("some_file.c", 3, argv);

where ``argv`` is a list of arguments to be passed to clang. It should almost
always include ``MatcherClangIncludePath`` for files like ``stddef.h`` to be
found properly.

File handles can be closed using ``matcher_close_file``:

    matcher_close_file(f);

The main way to use matchers is through the ``matcher_look_for`` function. It
returns a non-zero value if the matcher has matched at least one node in the
file's syntax tree:

     if (matcher_look_for(f, m, NULL, NULL)) { ... }

One can also pass a function pointer to ``matcher_look_for``. That function will
be called with:

1. The node that was matched
2. The name of the file where it matched
3. The line where it matched
4. The column where it matched
5. A set of capture groups
6. The 4th argument passed to ``matcher_look_for``

For example, this will log the location of every use of the ``sizeof`` operator:

    static void on_sizeof(CXCursor cursor, const char *filename,
                          unsigned int line, unsigned int column,
                          const matcher_captures *captures, void *user_data) {
       printf("%s:%u:%u", filename, line, column);
    }

    // ...
    matcher m = match_sizeof();
    matcher_look_for(f, m, on_sizeof, NULL);

Another way to use matchers is through the ``matcher_try`` function, to check if
a specfic node matches:

    /* cursor is a CXCursor, captures a pointer to a matcher_captures or NULL if
     * not needed. */
    if (matcher_try(m, cursor, captures)) { ... }

This is most useful when writing new matchers that combine exisiting ones.

Defining new matchers
---------------------

To define new matchers, you will most likely have to use ``libclang``
directly. Here is an example of the most trivial matchers you could write: one
that always matches:

    static int anything_try(CXCursor cursor, void *data,
                            matcher_captures *captures) { return 1; }
    static void anything_clean_up(void *data) {}
    matcher match_anything(void) {
      return (matcher){anything_try, anything_clean_up, NULL};
    }

Matchers have two function pointers. The first one (``anything_try`` in the
example) should return a non-zero value when it is called with a cursor that
should be matched. The second argument to that function is an arbitrary pointer
that is stored in the third element of the matcher structure. The last argument
is used for capture groups. Most likely, all you need to do is keep moving it
around.

The second function (``anything_clean_up``) is passed that pointer and should
release any resource created by the matcher. If the matcher is using existing
matchers, it should take ownership of them and therefore call
``matcher_release`` in its clean up function.

### match_result

``match_result(m)`` returns a matcher that will match, not only on ``m``, but
also on:

- uses of the ternary operator where the second or third operand are matched by
  ``m``;
- assignments where the right hand side is matched by ``m``;
- an expression matched by ``m`` placed in between parentheses;
- a typecast of an expression matched by ``m``.

``match_result`` also works recursively. For example, this means that if ``m``
matches ``expr``, it will also match ``((expr))``.

Those rules are intended to make it easy to write matchers without having to
worry about things that don't affect the semantics of code, such as
extra-parentheses.

Capture groups
--------------

Let's look at a rule previously written:

    matcher m = match_opseq(MatcherOpAdd,
                            match_pointer(),
                            match_opseq(MatcherOpMul,
                                        match_anything(),
                                        match_sizeof()));

Let us now imagine that we need to have access to a cursor on the pointer
and to a cursor on the product. A way to accomplish this is to use capture
groups. Simple change the rule as follows:

    #define PointerID 0
    #define ProductID 1

    matcher m = match_opseq(
      MatcherOpAdd,
      match_as(PointerID, match_pointer()),
      match_as(ProductID,
        match_opseq(MatcherOpMul,
                    match_anything(), match_sizeof())));

We can retrieve those cursors within a visitor function using the ``captures``
argument:

    static void on_match(CXCursor cursor, const char *filename,
                         unsigned int line, unsigned int column,
                         const matcher_captures *captures, void *user_data) {
       CXCursor pointer, product;
       if (matcher_get_capture(captures, PointerID, &pointer)) {
         /* executed only if the capture group is present */
       }
       matcher_get_capture(captures, ProductID, &product);

       /* ... */
    }
