#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "array.h"

int init_array(array *ary) {
#include "init_array.h"
}

void release_array(array *ary) {
#include "release_array.h"
}

int *get(array *ary, size_t i) {
#include "get.h"
}

int insert(array *ary, size_t i, int element) {
#include "insert.h"
}

int delete(array *ary, size_t i) {
#include "delete.h"
}
