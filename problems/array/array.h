#include <stddef.h>

typedef struct array {
  size_t size;
  size_t buffer_size;
  int *elements;
} array;

int  init_array(array *ary);
void release_array(array *ary);

int *get(array *ary, size_t i);
int insert(array *ary, size_t i, int element);
int delete(array *ary, size_t i);
