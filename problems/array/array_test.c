#include <check.h>
#include <stdio.h>
#include <matchers.h>
#include <check_helpers.h>
#include <stdlib_hooks.h>
#include <stdlib.h>

#include "array.h"

#define NUMBER_OF(array) ((sizeof(array)/sizeof(*(array))))

void sanity_check(array *ary, const char *ctxt) {
  ck_assert_msg(ary->size <= ary->buffer_size,
                "[%s] La capacité d’un tableau doit être supérieure ou égale à "
                "sa taille.",
                ctxt);
  ck_assert_msg(ary->buffer_size == 0 || ary->elements,
                "[%s] tableau->elements ne peut pas être NULL.",
                ctxt);
}

void check_array_contents(array *ary, size_t n, int *ref, const char *ctxt) {
  ck_assert_msg(ary->size == n,
                "[%s] La taille du tableau est %zu mais aurait dû valoir %zu",
                ctxt, ary->size, n);
  size_t i;
  for (i = 0; i < n; i++) {
    check_log("Accès à l’élément %zu dans un tableau de taille %zu",
              i, ary->size);
    if (ary->elements[i] != ref[i]) break;
  }

  if (i != n) {
    char contents[256];
    int str_i = 0;
    str_i += snprintf(contents+str_i, sizeof(contents)-str_i,
                      "%d", ref[0]);
    for (size_t j = 1; j < n; j++) {
      str_i += snprintf(contents+str_i, sizeof(contents)-str_i,
                        ", %d", ref[j]);
    }

    ck_assert_msg(0, "[%s] Le tableau contient %d à l’indice %zu, "
                  "le contenu attendu était {%s}",
                  ctxt, ary->elements[i], i, contents);
  }
}

NO_EARLY_EXIT_START_TEST(test_array_creation) {
  array ary;
  ck_assert_msg(init_array(&ary) == 0,
                "Créer un tableau ne devrait pas échouer.");
  ck_assert_msg(ary.size == 0, "Un nouveau tableau doit être vide.");
  sanity_check(&ary, "à la création du tableau");
  release_array(&ary);
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_insert) {
  array ary;
  ck_assert_msg(init_array(&ary) == 0);
  sanity_check(&ary, "à la création du tableau");

  size_t size = 1;
  int contents[] = {10,9,8,7,6,5,4,3,2,1,0};
  for (int i = 10; i >= 0; i--, size++) {
    check_log("Insertion à l’indice %zu dans un tableau de taille %zu",
              size-1, ary.size);
    ck_assert_msg(insert(&ary, size-1, i) == 0,
                  "Il doit être possible d’insérer un élément à la fin "
                  "d’un tableau");
    sanity_check(&ary, "après une insertion");
    check_array_contents(&ary, size, contents, "après une insertion");
  }

  check_log("Insertion à l’indice 2 pour un tableau de taille %zu",
            2, ary.size);
  ck_assert_msg(insert(&ary, 2, 42) == 0,
                "Il doit être possible d’insérer un élément au milieu "
                "d’un tableau");
  {
    int contents[] = {10,9,42,8,7,6,5,4,3,2,1,0};
    sanity_check(&ary, "après une insertion");
    check_array_contents(&ary, size, contents, "après une insertion");
  }

  release_array(&ary);
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_delete) {
  array ary;
  ck_assert_msg(init_array(&ary) == 0);
  sanity_check(&ary, "à la création du tableau");

  size_t size = 0;
  for (; size < 10; size++) {
    check_log("Insertion à l’indice %zu dans un tableau de taille %zu",
              size, ary.size);
    insert(&ary, size, size);
  }

  int contents[] = {0,1,2,3,4,5,6,7,8,9};
  sanity_check(&ary, "après une insertion invalide");
  check_array_contents(&ary, size, contents, "après une insertion");

  {
    check_log("Suppresion à l’indice 3 d’un tableau de taille %zu",
              ary.size);
    ck_assert_msg(delete(&ary, 3) == 0,
                  "Il doit être possible de supprimer un élément au milieu "
                  "d’un tableau.");
    int contents[] = {0,1,2,4,5,6,7,8,9};
    sanity_check(&ary, "après une suppression");
    check_array_contents(&ary, size-1, contents, "après une suppression");
  }

  {
    check_log("Suppresion à l’indice 0 d’un tableau de taille %zu",
              ary.size);
    ck_assert_msg(delete(&ary, 0) == 0,
                  "Il doit être possible de supprimer un élément au milieu "
                  "d’un tableau.");
    int contents[] = {1,2,4,5,6,7,8,9};
    sanity_check(&ary, "après une suppression");
    check_array_contents(&ary, size-2, contents, "après une suppression");
  }

  release_array(&ary);
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_invalid_insert_delete) {
  array ary;
  ck_assert_msg(init_array(&ary) == 0);
  sanity_check(&ary, "à la création du tableau");

  size_t size = 0;
  for (; size < 10; size++) insert(&ary, size, size);

  int contents[] = {0,1,2,3,4,5,6,7,8,9};
  sanity_check(&ary, "après une insertion");
  check_array_contents(&ary, size, contents, "après une insertion");

#define wrong_insertion(index, value)                                   \
  do {                                                                  \
    check_log("Insertion à l’indice %zu dans un tableau de taille %zu", \
              index, ary.size);                                         \
    ck_assert_msg(insert(&ary, size+1, 10) < 0,                         \
                  "Il est interdit d’insérer un élément en dehors "     \
                  "du tableau");                                        \
    sanity_check(&ary, "après une insertion invalide");                 \
    check_array_contents(&ary, size, contents,                          \
                         "après une insertion invalide");               \
  } while (0)

#define wrong_deletion(index) \
  do { \
    check_log("Suppression à l’indice %zu d’un tableau de taille %zu",  \
              index, ary.size);                                         \
    ck_assert_msg(delete(&ary, index) < 0,                              \
                  "Il est interdit de supprimer un élément en dehors"   \
                  " du tableau");                                       \
    sanity_check(&ary, "après une suppression invalide");               \
    check_array_contents(&ary, size, contents,                          \
                         "après une suppression invalide");             \
  } while(0)

  wrong_insertion(size+1, 10);
  wrong_insertion(size+15, 20);
  wrong_insertion(size+30, 15);

  wrong_deletion(size);
  wrong_deletion(size+15);
  wrong_deletion(size+17);
  wrong_deletion(size+25);

#undef wrong_insertion
#undef wrong_deletion

  release_array(&ary);
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_get) {
  array ary;
  ck_assert_msg(init_array(&ary) == 0);
  sanity_check(&ary, "à la création du tableau");

  int size = 0;
  int contents[] = {0,10,20,50,30,43,60,70,80,90};
  for (; size < 10; size++) insert(&ary, size, contents[size]);
  sanity_check(&ary, "après une insertion");
  check_array_contents(&ary, size, contents, "après une insertion");

  for (int i = 0; i < size; i++) {
    check_log("Appel à get(&tableau, %d) avec un tableau de taille %zu",
              i, ary.size);
    ck_assert_msg(get(&ary, i), "Un pointeur valide doit être renvoyé pour un "
                  "appel à get au milieu du tableau");
    ptrdiff_t x = get(&ary, i) - ary.elements;
    ck_assert_msg(x == i, "get(&tableau, %d) renvoie un pointeur vers "
                  "l’élément d’indice %d", i, x);
  }

#define invalid_get(index)                                                     \
  do {                                                                         \
    check_log("Appel à get(&tableau, %zu) pour un tableau de taille %zu",      \
              index, ary.size);                                                \
    ck_assert_msg(get(&ary, index) == NULL,                                    \
                  "get(&tableau, i) en dehors du tableau doit renvoyer NULL"); \
  } while(0)

  invalid_get(size);
  invalid_get(100);
  invalid_get(20);
  invalid_get(30);

#undef invalid_get
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_create_fail) {
  check_log("Création d’un tableau lorsque malloc va échouer");

  hook_alloc_force_fail();

  array ary;
  if (init_array(&ary) == 0) {
    if (ary.buffer_size != 0) {
      hook_alloc_stop_fail();
      ck_assert_msg(0, "init_array prétend avoir créé un buffer non-vide alors que malloc a échoué");
    }

    if (insert(&ary, 0, 0) == 0) {
      hook_alloc_stop_fail();
      ck_assert_msg(0, "insert prétend avoir créé un buffer non-vide alors que malloc a échoué");
    }
  }

  hook_alloc_stop_fail();
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_insert_fail) {
  array ary;
  ck_assert_msg(init_array(&ary) == 0);
  sanity_check(&ary, "à la création du tableau");

  int size = 0;
  int contents[] = {0,10,20,50,30,43,60,70,80,90};
  for (; size < 10; size++) insert(&ary, size, contents[size]);
  sanity_check(&ary, "après une insertion");
  check_array_contents(&ary, size, contents, "après une insertion");

  /* Force the array to grow when realloc/malloc fail */
  hook_alloc_force_fail();
  size_t n = ary.buffer_size - size;
  size_t i;
  for (i = 0; i < n+1; i++) {
    if (insert(&ary, 0, i) < 0) break;
  }
  hook_alloc_stop_fail();

  ck_assert_msg(i < n+1, "Votre implémentation prétend avoir augmenté la "
                "capacité du tableau alors que malloc a échoué.");

  sanity_check(&ary, "après une insertion n’ayant pas réussi à "
               "allouer de la mémoire");
} NO_EARLY_EXIT_END_TEST

static void on_wrong_pointer_math(CXCursor cursor, const char *filename,
                                  unsigned int line, unsigned int column,
                                  const matcher_captures *captures,
                                  void *user_data) {
  fprintf(stderr, "%s:%u:%u: Rappel: un pointeur vers le ième élément de "
          "array vaut array+i, pas array+i*sizeof(int)\n",
          filename, line, column);
}

static void on_bad_realloc(CXCursor cursor, const char *filename,
                           unsigned int line, unsigned int column,
                           const matcher_captures *captures, void *user_data) {
  fprintf(stderr, "%s:%u:%u: Rappel: x = realloc(x, n) cause un memory leak "
          "quand realloc échoue. Utilisez une variable intermédiaire.\n",
          filename, line, column);
}

NO_EARLY_EXIT_START_TEST(test_syntax_usage) {
  const char *args[] = {MatcherClangIncludePath};
  matcher_file f = matcher_open("array.c", NUMBER_OF(args), args);

  matcher wrong_pointer_math = match_opseq(MatcherOpAdd,
                                           match_pointer(),
                                           match_opseq(MatcherOpMul,
                                                       match_anything(),
                                                       match_sizeof()));
  matcher_look_for(f, wrong_pointer_math, on_wrong_pointer_math, NULL);
  matcher_release(wrong_pointer_math);

  matcher bad_array_syntax = match_in_function("get", match_array_subscript());
  ck_assert_msg(!matcher_look_for(f, bad_array_syntax, NULL, NULL),
                "Vous ne pouvez pas utiliser la syntaxe d’accès aux tableaux "
                "dans la fonction get");
  matcher_release(bad_array_syntax);

  matcher realloc_args[] = {
    match_member_ref(match_or(match_by_typename("struct array"),
                              match_by_typename("struct array *")), "elements"),
    match_anything()
  };

  matcher bad_realloc = match_binop(
    MatcherOpAssign,
    match_member_ref(match_or(match_by_typename("struct array"),
                              match_by_typename("struct array *")), "elements"),
    match_call("realloc", 2, realloc_args));
  matcher_look_for(f, bad_realloc, on_bad_realloc, NULL);
  matcher_release(bad_realloc);

  matcher_close_file(f);
} NO_EARLY_EXIT_END_TEST

Suite *array_suite(void) {
  Suite *s = suite_create("Array");

  TCase *tc_create = tcase_create("Création du tableau");
  tcase_add_logging_test(tc_create, test_array_creation);
  suite_add_tcase(s, tc_create);

  TCase *tc_insert = tcase_create("Modification du tableau");
  tcase_add_logging_test(tc_insert, test_insert);
  tcase_add_logging_test(tc_insert, test_delete);
  tcase_add_logging_test(tc_insert, test_invalid_insert_delete);
  suite_add_tcase(s, tc_insert);

  TCase *tc_get = tcase_create("Lecture du tableau");
  tcase_add_logging_test(tc_get, test_get);
  suite_add_tcase(s, tc_get);

  TCase *tc_fail = tcase_create("Gestion des erreurs");
  tcase_add_logging_test(tc_fail, test_create_fail);
  tcase_add_logging_test(tc_fail, test_insert_fail);
  suite_add_tcase(s, tc_fail);

  TCase *tc_syntax = tcase_create("Syntaxe");
  tcase_add_logging_test(tc_syntax, test_syntax_usage);
  suite_add_tcase(s, tc_syntax);

  return s;
}

int main(int argc, char **argv) {
  if (check_early_exit_setup() < 0) {
    fprintf(stderr, "Failed to setup early exit testing.\n");
    return EXIT_FAILURE;
  }

  int number_failed;
  Suite *s;
  SRunner *sr;

  s = array_suite();
  sr = srunner_create(s);

  check_init_logs();
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  number_failed = check_early_exit(number_failed);
  check_print_logs();
  srunner_free(sr);

  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
