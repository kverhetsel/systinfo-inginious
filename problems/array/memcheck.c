#include "array.h"
#include <stdlib.h>
#include <stdlib_hooks.h>
#include <stdio.h>

#define max(a, b) ((a) > (b) ? (a) : (b))

#define N_INSERTION 1024
#define N_DELETION  512

int main(int argc, char **argv) {
  /* Always use the same seed for consistency. */
  srand(0);

  array ary;
  init_array(&ary);

  size_t size = 0;

  for (size_t i = 0; i < N_INSERTION; i++) {
    size_t valid_j   = rand() % (size+1);
    size_t invalid_j = (size+1) + rand() % (size+1);

    insert(&ary, invalid_j, i);
    insert(&ary, valid_j, i);

    size++;
  }

  /*
   * In case a large initial buffer size was picked, force the code that
   * grows it to run at least once.
   */
  size_t n = max((ary.buffer_size - ary.size) * 2, 100);
  for (size_t i = 0; i < n; i++) {
    size_t valid_j   = rand() % (size+1);
    size_t invalid_j = (size+1) + rand() % (size+1);

    insert(&ary, invalid_j, i);
    insert(&ary, valid_j, i);

    size++;
  }


  for (size_t i = 0; i < N_DELETION; i++) {
    size_t valid_j   = rand() % size;
    size_t invalid_j = size + rand() % size;

    delete(&ary, invalid_j);
    delete(&ary, valid_j);

    size--;
  }

  for (size_t i = 0; i < size; i++) *get(&ary, i) *= 2;
  for (size_t i = 0; i < 1024; i++) get(&ary, size + i);

  release_array(&ary);

  hook_alloc_force_fail();
  if (init_array(&ary) == 0) {
    insert(&ary, 0, 3);
    release_array(&ary);
  }
  hook_alloc_stop_fail();

  init_array(&ary);
  for (size_t i = 0; i < 10; i++) insert(&ary, i, i);
  hook_alloc_force_fail();
  n = ary.buffer_size - ary.size;
  for (size_t i = 0; i < n+1; i++) insert(&ary, 0, i);
  hook_alloc_stop_fail();
  release_array(&ary);

  return 0;
}
