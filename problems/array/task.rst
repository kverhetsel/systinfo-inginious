=====
Array
=====

:author: Kilian Verhetsel
:environment: systinfo
:name: array
:order: 0
:limit-time: 10
:limit-memory: 128
:limit-output: 5210

Dans cette question, il vous est demandé d’écrire des fonctions permettant de
manipuler des tableaux de taille variable. Pour ce faire, la structure suivante
sera utilisée::

      typedef struct array {
        size_t size; /* La taille du tableau. */
        size_t buffer_size; /* Le nombre d’éléments alloués. */
        int *elements; /* Le contenu du tableau. */
      } array;

Remarquez que pour une implémentation efficace, ``buffer_size`` n’est pas
toujours égal à ``size``.

Pour mieux comprendre les fonctions que vous devez définir, voici un exemple de
programme qui les utilise (la vérification des erreurs à été omise par souci de
clarté)::

    array ary;
    init_array(&ary); /* Crée un nouveau tableau vide. */
    insert(&ary, 0, 74); /* Contenu du tableau: 74 */
    insert(&ary, 0, 100); /* Contenu du tableau: 100, 74 */
    insert(&ary, 2, 200); /* Contenu du tableau: 100, 74, 200 */
    insert(&ary, 2, 150); /* Contenu du tableau: 100, 74, 150, 200 */
    delete(&ary, 1); /* Contenu du tableau: 100, 150, 200 */
    *get(&ary, 1) = 120; /* Contenu du tableau: 100, 120, 200 */
    printf("%d\n", *get(&ary, 0)); /* Affiche 0 */
    release_array(&ary); /* Libère la mémoire utilisée par le tableau. */

Création du tableau
===================

.. question:: init_array
   :type: code
   :language: c

   Écrivez le corps de la fonction ``int init_array(array *ary)``. Celle-ci
   doit modifier la structure pointée par son argument pour créer un nouveau
   tableau vide.

   La valeur de retour de la fonction doit être ``0`` si tout va bien et une
   valeur négative si elle n’arrive pas à allouer la mémoire dont elle a
   besoin.

Libération de la mémoire
========================

.. question:: release_array
   :type: code
   :language: c

   Écrivez le corps de la fonction ``void release_array(array *ary)``, qui doit
   libérer toute la mémoire utilisée par le tableau.

Lecture des données
===================

.. question:: get
   :type: code
   :language: c

   Écrivez le corps de la fonction ``int *get(array *ary, size_t i)``, qui
   renvoie un pointeur vers le ième élément du tableau, de manière à ce que la
   fonction puisse être utilisée pour lire une valeur aussi bien que pour en
   modifier une.

   Si l’élément d’indice ``i`` est en dehors du tableau, il vous est demandé de
   renvoyer un pointeur nul, de manière à rendre la détection des erreurs plus
   facile pour l’utilisateur de votre programme.

   Pour cette question (et uniquement celle-ci), vous ne pouvez pas utiliser la
   notation des tableaux en C (``array[i]``). À la place, il vous est demandé
   d’utiliser l’arithmétique des pointeurs pour que votre compréhension en soit
   vérifiée.

Insertion d’un élément dans le tableau
======================================

.. question:: insert
   :type: code
   :language: c

   Écrivez le corps de la fonction ``int insert(array *ary, size_t i, int
   element)``. Celle-ci doit rajouter un élément juste avant l’élément qui se
   trouve pour l’instant à l’indice ``i``, de sorte que ``*get(ary, i) ==
   element`` soit vrai après l’exécution de la fonction.

   Si ``i`` vaut la taille du tableau, il faut rajouter un élément à la fin du
   tableau.

   La fonction doit renvoyer zéro si tout a fonctionné et une valeur négative si
   elle n’a pas réussi à allouer la mémoire dont elle a besoin ou si ``i`` est
   supérieur à la taille du tableau (auquel cas ce dernier ne doit pas être
   modifié).


Suppression d’un élément du tableau
===================================

.. question:: delete
   :type: code
   :language: c

   Écrivez le corps de la fonction ``int delete(array *ary, size_t
   i)``. Celle-ci doit supprimer l’élément du tableau se trouvant à l’indice
   ``i``.

   Si ``i`` se trouve en dehors du tableau, la fonction doit renvoyer une valeur
   négative et ne pas modifier le tableau. Sinon, elle doit renvoyer zéro.
