#include <check.h>
#include <stdio.h>
#include <matchers.h>
#include <check_helpers.h>
#include <stdlib_hooks.h>
#include <stdlib.h>
#include "free2d.h"

int **make_array(size_t n_lines, size_t n_columns);

NO_EARLY_EXIT_START_TEST(test_both) {
  for (size_t i = 0; i < 10; i++) {
    size_t expected_rows = 1 + (rand() % 100);
    size_t expected_columns = 1 + (rand() % 100);

    check_log("Appel à make_array(%zu, %zu)", expected_rows, expected_columns);
    int **array = make_array(expected_rows, expected_columns);
    ck_assert_msg(array != NULL,
                  "make_array renvoie NULL alors que malloc n’a pas échoué");
    for (size_t i = 0; i < expected_rows; i++) {
      check_log("Accès à la ligne %zu d’un tableau à %zu lignes",
                i, expected_rows);
      ck_assert_msg(array[i] != NULL, "Le pointeur sur la ligne %zu est nul");
    }
    for (size_t i = 1; i < expected_rows; i++) {
      check_log("Accès aux lignes %zu et %zu d’un tableau à %zu lignes",
                i, i-1, expected_rows);
      ck_assert_msg(array[i] - array[i-1] == (ptrdiff_t)expected_columns,
                    "Le décalage entre les lignes %zu et %zu est %d alors "
                    "qu’il y a %zu colonnes dan le tableau : le tableau n’est "
                    "pas sur une zone contiguë de la mémoire.",
                    i, i-1, array[i] - array[i-1],
                    expected_columns);
    }

    for (size_t i = 0; i < expected_rows; i++) {
      for (size_t j = 0; j < expected_columns; j++) {
        check_log("Écriture dans array[%zu][%zu] pour un tableau de "
                  "taille %zux%zu", i, j, expected_rows, expected_columns);
        array[i][j] = i*expected_columns+j;
      }
    }

    for (size_t i = 0; i < expected_rows; i++) {
      for (size_t j = 0; j < expected_columns; j++) {
        check_log("Lecture depuis array[%zu][%zu] pour un tableau de "
                  "taille %zux%zu", i, j, expected_rows, expected_columns);
        ck_assert_msg(
          array[i][j] == (int)(i*expected_columns+j),
          "L’élément à la ligne %zu et à la colonne %zu n’a pas préservé la "
          "valeur qui y avait été stockée.", i, j);
      }
    }

    free2d(array, expected_rows);
  }
} NO_EARLY_EXIT_END_TEST

static size_t fail_after = 0;

static void *deferred_failing_malloc(size_t n) {
  if (fail_after > 0) fail_after--;
  return fail_after == 0 ? NULL : hook_real_malloc(n);
}

NO_EARLY_EXIT_START_TEST(test_both_failure) {
  for (size_t i = 0; i < 2; i++) {
    size_t expected_rows = 1 + (rand() % 100);
    size_t expected_columns = 1 + (rand() % 100);

    check_log("Appel à make_array(%zu, %zu) où malloc échouera",
              expected_rows, expected_columns);

    fail_after = i;
    hook_current_malloc = deferred_failing_malloc;
    int **pointer = make_array(expected_rows, expected_columns);
    hook_current_malloc = NULL;

    ck_assert_msg(pointer == NULL,
                  "make_array(%zu, %zu) ne renvoie pas NULL alors que "
                  "malloc a échoué", expected_rows, expected_columns);
  }
} NO_EARLY_EXIT_END_TEST

Suite *naive_suite(void) {
  Suite *s = suite_create("Combinaison des deux méthodes");

  TCase *tc_core = tcase_create("Core");
  tcase_add_logging_test(tc_core, test_both);
  tcase_add_logging_test(tc_core, test_both_failure);
  suite_add_tcase(s, tc_core);

  return s;
}

int main(int argc, char **argv) {
  if (check_early_exit_setup() < 0) {
    fprintf(stderr, "Failed to setup early exit testing.\n");
    return EXIT_FAILURE;
  }

  srand(0);

  Suite *s = naive_suite();
  SRunner *sr = srunner_create(s);

  check_init_logs();
  srunner_run_all(sr, CK_NORMAL);
  int number_failed = srunner_ntests_failed(sr);
  number_failed = check_early_exit(number_failed);
  check_print_logs();
  srunner_free(sr);

  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
