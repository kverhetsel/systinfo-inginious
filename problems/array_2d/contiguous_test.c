#include <check.h>
#include <stdio.h>
#include <matchers.h>
#include <check_helpers.h>
#include <stdlib_hooks.h>
#include <stdlib.h>

int *make_array(size_t n_lines, size_t n_columns);

NO_EARLY_EXIT_START_TEST(test_contiguous) {
  for (size_t i = 0; i < 10; i++) {
    size_t expected_rows = 1 + (rand() % 100);
    size_t expected_columns = 1 + (rand() % 100);

    check_log("Appel à make_array(%zu, %zu)", expected_rows, expected_columns);
    int *array = make_array(expected_rows, expected_columns);
    ck_assert_msg(array != NULL,
                  "make_array renvoie NULL alors que malloc n’a pas échoué");

    for (size_t i = 0; i < expected_rows; i++) {
      for (size_t j = 0; j < expected_columns; j++) {
        check_log("Écriture dans la ligne %zu et colonne %zu "
                  "pour un tableau de taille %zux%zu",
                  i, j, expected_rows, expected_columns);
        array[i*expected_columns+j] = i*expected_columns+j;
      }
    }

    for (size_t i = 0; i < expected_rows; i++) {
      for (size_t j = 0; j < expected_columns; j++) {
        check_log("Lecture depuis la ligne %zu et colonne %zu "
                  "pour un tableau de taille %zux%zu",
                  i, j, expected_rows, expected_columns);
        ck_assert_msg(
          array[i*expected_columns+j] == (int)(i*expected_columns+j),
          "L’élément à la ligne %zu et à la colonne %zu n’a pas préservé la "
          "valeur qui y avait été stockée.", i, j);
      }
    }

    free(array);
  }
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_contiguous_failure) {
  size_t expected_rows = 1 + (rand() % 100);
  size_t expected_columns = 1 + (rand() % 100);

  check_log("Appel à make_array(%zu, %zu) où malloc échouera",
            expected_rows, expected_columns);
  hook_alloc_force_fail();
  int *pointer = make_array(expected_rows, expected_columns);
  hook_alloc_stop_fail();

  ck_assert_msg(pointer == NULL,
                "make_array(%zu, %zu) ne renvoie pas NULL alors que "
                "malloc a échoué", expected_rows, expected_columns);
} NO_EARLY_EXIT_END_TEST

Suite *naive_suite(void) {
  Suite *s = suite_create("Mémoire contiguë");

  TCase *tc_core = tcase_create("Core");
  tcase_add_logging_test(tc_core, test_contiguous);
  tcase_add_logging_test(tc_core, test_contiguous_failure);
  suite_add_tcase(s, tc_core);

  return s;
}

int main(int argc, char **argv) {
  if (check_early_exit_setup() < 0) {
    fprintf(stderr, "Failed to setup early exit testing.\n");
    return EXIT_FAILURE;
  }

  srand(0);

  Suite *s = naive_suite();
  SRunner *sr = srunner_create(s);

  check_init_logs();
  srunner_run_all(sr, CK_NORMAL);
  int number_failed = srunner_ntests_failed(sr);
  number_failed = check_early_exit(number_failed);
  check_print_logs();
  srunner_free(sr);

  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
