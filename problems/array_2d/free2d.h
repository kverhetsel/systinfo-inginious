#include <stdlib.h>
#include <stddef.h>

#ifdef CONTIGUOUS
static void free2d(int **array, size_t n_rows) {
  free(array[0]);
  free(array);
}
#else
static void free2d(int **array, size_t n_rows) {
  for (size_t i = 0; i < n_rows; i++) free(array[i]);
  free(array);
}
#endif
