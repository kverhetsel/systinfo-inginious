#include <stdlib_hooks.h>
#include <stdlib.h>

int *make_array(size_t n_lines, size_t n_columns);

int main(int argc, char **argv) {
  for (size_t i = 0; i < 100; i++) {
    size_t expected_rows = 1 + (rand() % 100);
    size_t expected_columns = 1 + (rand() % 100);

    int *array = make_array(expected_rows, expected_columns);

    for (size_t i = 0; i < expected_rows; i++) {
      for (size_t j = 0; j < expected_columns; j++) {
        array[i*expected_columns+j] = i*expected_columns+j;
      }
    }

    free(array);
  }

  hook_alloc_force_fail();
  free(make_array(100, 50));
  hook_alloc_stop_fail();

  return 0;
}
