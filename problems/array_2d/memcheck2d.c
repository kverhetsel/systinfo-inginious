#include "free2d.h"
#include <stdlib_hooks.h>

int **make_array(size_t n_lines, size_t n_columns);

static size_t fail_after = 0;

static void *deferred_failing_malloc(size_t n) {
  if (fail_after > 0) fail_after--;
  return fail_after == 0 ? NULL : hook_real_malloc(n);
}

int main(int argc, char **argv) {
  for (size_t i = 0; i < 100; i++) {
    size_t expected_rows = 1 + (rand() % 100);
    size_t expected_columns = 1 + (rand() % 100);

    int **array = make_array(expected_rows, expected_columns);

    for (size_t i = 0; i < expected_rows; i++) {
      for (size_t j = 0; j < expected_columns; j++) {
        array[i][j] = i*expected_columns+j;
      }
    }

    free2d(array, expected_rows);
  }

  hook_current_malloc = deferred_failing_malloc;
  for (size_t i = 0; i < 101; i++) {
    int **pointer = make_array(100, 50);
    if (pointer) free2d(pointer, 100);
  }
  hook_current_malloc = NULL;

  return 0;
}
