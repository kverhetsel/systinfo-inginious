============================
 Tableaux à deux dimensions
============================

:author: Kilian Verhetsel
:environment: systinfo
:name: array_2d
:order: 3
:limit-time: 10
:limit-memory: 128
:limit-output: 5210

Le C possède une syntaxe pour définir des tableaux à plusieurs dimensions. Par
exemple, la ligne suivante déclare un tableau ``x`` à deux dimensions, avec 5
lignes et 3 colonnes::

  int x[5][3];

La ligne suivante permet alors d’afficher l’élément à la ligne ``i`` et à la
colonne ``j``::

  printf("%d\n", x[i][j]);

Dans cette question, nous allons explorer les différentes approches pour créer
dynamiquement des tableaux à plusieurs dimensions à l’aide de ``malloc``.

La solution naïve
=================

.. question:: naive
   :type: code
   :language: c

   Une solution simple est d’utiliser ``malloc`` pour allouer un tableau de
   pointeurs. Chaque case correspond à une ligne du tableau à deux
   dimensions. Les lignes sont allouées avec ``malloc`` et le pointeur vers le
   début de chaque ligne est placé dans la case correspondante du premier
   tableau.

   Écrivez le corps de la fonction ``int **make_array(size_t n_lines, size_t
   n_columns)`` qui renvoie une tableau d’entiers à 2 dimensions, avec
   ``n_lines`` lignes et ``n_columns`` colonnes.

   .. TODO: Insert naive.png

Allouer de la mémoire contiguë
==============================

.. question:: contiguous
   :type: code
   :language: c

   Une différence importante entre le tableau créé à la question précédente et
   la notation des tableaux en C est qu’avec cette dernière toutes les lignes
   sont placées l’une après l’autre en mémoire, sans espace entre les
   lignes. Allouer les lignes avec ``malloc`` n’offre pas cette garantie.

   Une approche pour résoudre le problème consiste à n’utiliser ``malloc``
   qu’une seule fois pour créer un tableau à une dimension et de taille
   ``n_lines * n_columns``.

   Écrivez le corps de la fonction ``int *make_array(size_t n_lines, size_t
   n_columns)`` qui utilise cette méthode pour créer un tableau à 2 dimensions,
   avec ``n_lines`` lignes et ``n_columns`` colonnes.

   .. TODO: Insert contiguous.png

Combiner les deux solutions
===========================

.. question:: both
   :type: code
   :language: c

   Le tableau créé dans la question précédente n’a qu’une seule dimension et la
   notation ``x[i][j]`` ne peut donc pas être utilisée pour accéder à ses
   éléments. De plus, un tel tableau ne peut pas à être passé à une fonction qui
   attend un tableau tel que ceux créés lors de la première question.

   Il est possible de résoudre le problème en combinant les deux méthodes : on
   alloue un tableau de pointeurs avec ``malloc`` et un autre tableau qui
   contiendra tous les éléments du tableau à deux dimension, avec un seul autre
   appel à ``malloc``. Il suffit alors de calculer l’adresse du début de chaque
   ligne et de la placer dans la case correspondante du premier tableau.

   Écrivez le corps de la fonction ``int **make_array(size_t n_lines, size_t
   n_columns)`` qui implémente cette stratégie.

   .. TODO: Insert both.png
