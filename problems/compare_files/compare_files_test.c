#include <check_helpers.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#include "utils.h"

ssize_t compare_files(const char *a, const char *b);

NO_EARLY_EXIT_START_TEST(test_compare_files) {
  for (size_t i = 0; i < 10; i++) {
    size_t a = rand() % 4096;
    size_t b = rand() % 4096;

    size_t equal_count;
    write_two_files("foo", a, "bar", b, &equal_count);

    check_log("Appel à compare_files");
    ssize_t ret = compare_files("foo", "bar");
    ck_assert_msg(ret >= 0, "compare_files renvoie une erreur lors "
                  "d’un appel valide.");
    ck_assert_msg((size_t)ret == equal_count,
                  "compare_files renvoie %ld alors qu’il y a %zu bytes égaux",
                  ret, equal_count);
  }
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_no_a) {
  check_log("Appel à compare_files alors que le premier fichier "
            "n’existe pas.");
  ssize_t ret = compare_files("no_foo", "bar");
  ck_assert_msg(ret < 0, "compare_files ne renvoie pas d’erreur alors "
               "que le premier fichier n’existe pas.");
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_no_b) {
  check_log("Appel à compare_files alors que le second fichier "
            "n’existe pas.");
  ssize_t ret = compare_files("foo", "no_bar");
  ck_assert_msg(ret < 0, "compare_files ne renvoie pas d’erreur alors "
               "que le second fichier n’existe pas.");
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_neither) {
  check_log("Appel à compare_files alors qu’aucun des deux fichiers "
            "n’existe.");
  ssize_t ret = compare_files("no_foo", "no_bar");
  ck_assert_msg(ret < 0, "compare_files ne renvoie pas d’erreur alors "
               "qu’aucun des deux fichiers n’existent.");
} NO_EARLY_EXIT_END_TEST

Suite *compare_files_suite(void) {
  Suite *s = suite_create("compare_files");

  TCase *tc_core = tcase_create("Core");
  tcase_add_logging_test(tc_core, test_compare_files);
  tcase_add_logging_test(tc_core, test_no_a);
  tcase_add_logging_test(tc_core, test_no_b);
  tcase_add_logging_test(tc_core, test_neither);
  suite_add_tcase(s, tc_core);

  return s;
}

int main(int argc, char **argv) {
  if (check_early_exit_setup() < 0) {
    fprintf(stderr, "Failed to setup early exit testing.\n");
    return EXIT_FAILURE;
  }

  srand(0);

  Suite *s = compare_files_suite();
  SRunner *sr = srunner_create(s);

  check_init_logs();
  srunner_run_all(sr, CK_NORMAL);
  int number_failed = srunner_ntests_failed(sr);
  number_failed = check_early_exit(number_failed);
  check_print_logs();
  srunner_free(sr);

  return number_failed == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}
