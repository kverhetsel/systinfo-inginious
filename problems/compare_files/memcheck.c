#include <sys/types.h>

#include "utils.h"
ssize_t compare_files(const char *a, const char *b);

int main(int argc, char **argv) {
  for (size_t i = 0; i < 10; i++) {
    size_t size_a = rand() % 4096;
    size_t size_b = rand() % 4096;

    write_two_files("a", size_a, "b", size_b, NULL);
    compare_files("a", "b");
  }

  compare_files("a", "no_b");
  compare_files("no_a", "b");
  compare_files("no_a", "no_b");

  return 0;
}
