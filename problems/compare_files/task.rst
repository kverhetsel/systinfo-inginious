=========================
 Comparer deux fichiers
=========================

:author: Kilian Verhetsel
:environment: systinfo
:name: compare_files
:order: 9
:limit-time: 10
:limit-memory: 128
:limit-output: 5210

Dans cet exercice, vous devez ouvrir deux fichiers et les comparer : vous devez
compter le nombre de bytes qui se trouvent à la même position dans chacun des
deux fichiers et ont la même valeur.

Par exemple, avec le fichier suivant::

  aacaca

et celui-là::

  abaabbaaaaaaaa

Le résultat devrait être 2 car les deux fichiers contiennent le même bytes en
première et en quatrième position.

Comparaison de deux fichiers
============================

.. question:: compare_files
   :type: code
   :language: c

   Écrivez le corps de la fonction ``ssize_t compare_files(const char *a, const
   char *b)`` qui compare les deux fichiers dont le chemin est passé en argument
   en utilisant la méthode décrite précédemment. En cas d’erreur, votre fonction
   doit renvoyer -1.
