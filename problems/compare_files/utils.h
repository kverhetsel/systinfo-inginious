#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>

static
void write_two_files(const char *name_a, size_t size_a,
                     const char *name_b, size_t size_b,
                     size_t *out_equal_count) {
  size_t equal_count = 0;

  unsigned char data_a[size_a];
  unsigned char data_b[size_b];

  size_t min = size_a < size_b ? size_a : size_b;
  for (size_t i = 0; i < min; i++) {
    unsigned char a = rand() % 256;
    unsigned char b = rand() % 256;

    if (a == b) equal_count++;

    data_a[i] = a;
    data_b[i] = b;
  }

  if (size_a > size_b) {
    for (size_t i = size_b; i < size_a; i++)
      data_a[i] = rand() % 256;
  }
  else {
    for (size_t i = size_a; i < size_b; i++)
      data_b[i] = rand() % 256;
  }

  FILE *f;

  f = fopen(name_a, "w");
  fwrite(data_a, 1, size_a, f);
  fclose(f);

  f = fopen(name_b, "w");
  fwrite(data_b, 1, size_b, f);
  fclose(f);

  if (out_equal_count) *out_equal_count = equal_count;
}
