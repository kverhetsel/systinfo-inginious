#ifndef DEFS_H_
#define DEFS_H_

typedef struct expression {
  float (*eval)(const void *expr);
  void (*release)(void *expr);
} expression;

expression *constant(float n);

expression *add(expression *a, expression *b);
expression *subtract(expression *a, expression *b);
expression *multiply(expression *a, expression *b);
expression *divide(expression *a, expression *b);

float eval(const expression *expr);

void free_expression(expression *expr);

#endif
