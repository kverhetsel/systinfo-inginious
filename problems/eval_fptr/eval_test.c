#include "defs.h"
#include <check_helpers.h>
#include <stdlib.h>
#include <stdlib_hooks.h>
#include <stdio.h>
#include <math.h>

static expression *mk_constant(float n) {
  check_log("Création d’une constante");
  expression *e = constant(n);

  ck_assert_msg(e != NULL, "constant renvoie NULL alors que malloc "
                  "n’a pas échoué.");
  return e;
}

#define def_binop(operation)                                  \
  static expression *do_##operation(expression *a, expression *b) {       \
    check_log("Appel à "#operation);                                      \
    expression *e = operation(a, b);                                      \
    ck_assert_msg(e != NULL, #operation " renvoie NULL alors que malloc " \
                  "n’a pas échoué");                                      \
    return e;                                                             \
  }

def_binop(add)
def_binop(subtract)
def_binop(multiply)
def_binop(divide)

#undef def_binop

static void do_free(expression *expr) {
  check_log("Appel à free_expression");
  free_expression(expr);
}

static void check_value_f(expression *expr,
                          const char *expr_name, float expected) {
  check_log("Évaluation de l’expression %s");
  float actual = eval(expr);
  ck_assert_msg(fabsf(actual - expected) < 2e-7,
                "L’évaluation de l’expression %s donne %f au lieu de %f",
                expr_name,  actual, expected);
  do_free(expr);
}

#define check_value(expr, value) check_value_f((expr), #value, (value))

NO_EARLY_EXIT_START_TEST(test_create) {
  do_free(mk_constant(2));
  do_free(mk_constant(3.5));
  do_free(do_add(mk_constant(20.0), mk_constant(0)));
  do_free(do_divide(mk_constant(1),
                    do_multiply(mk_constant(1.41), mk_constant(1.41))));
  do_free(do_divide(mk_constant(1),
                    do_add(mk_constant(1.41), mk_constant(1.41))));
  do_free(do_subtract(mk_constant(1),
                      do_add(mk_constant(1.0), mk_constant(2.0))));
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_create_no_malloc) {
  expression *a = mk_constant(4.0);
  expression *b = mk_constant(5.0);

  expression *e;

  hook_alloc_force_fail();
  e = constant(1.0);
  hook_alloc_stop_fail();
  ck_assert_msg(e == NULL, "constant ne renvoie pas NULL alors que "
                "malloc a échoué");

  hook_alloc_force_fail();
  e = subtract(a, b);
  hook_alloc_stop_fail();
  ck_assert_msg(e == NULL, "subtract ne renvoie pas NULL alors que "
                "malloc a échoué");

  hook_alloc_force_fail();
  e = multiply(a, b);
  hook_alloc_stop_fail();
  ck_assert_msg(e == NULL, "multiply ne renvoie pas NULL alors que "
                "malloc a échoué");

  hook_alloc_force_fail();
  e = divide(a, b);
  hook_alloc_stop_fail();
  ck_assert_msg(e == NULL, "divide ne renvoie pas NULL alors que "
                "malloc a échoué");

  do_free(a);
  do_free(b);
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_eval) {
  check_value(mk_constant(2), 2);
  check_value(mk_constant(3.5), 3.5);
  check_value(do_add(mk_constant(20.0), mk_constant(0)), 20.0 + 0.0);
  check_value(do_divide(mk_constant(1),
                        do_multiply(mk_constant(1.41), mk_constant(1.41))),
              1.0/(1.41*1.41));
  check_value(do_divide(mk_constant(1),
                        do_add(mk_constant(20), mk_constant(21))),
              1.0/(20+21));
  check_value(do_subtract(mk_constant(1),
                          do_add(mk_constant(1.0), mk_constant(2.0))),
              1.0 - (1.0 + 2.0));
} NO_EARLY_EXIT_END_TEST

Suite *eval_suite(void) {
  Suite *s = suite_create("Eval");

  TCase *tc_create = tcase_create("Création d’une expression");
  tcase_add_logging_test(tc_create, test_create);
  tcase_add_logging_test(tc_create, test_create_no_malloc);
  suite_add_tcase(s, tc_create);

  TCase *tc_eval = tcase_create("Évaluation d’une expression");
  tcase_add_logging_test(tc_eval, test_eval);
  suite_add_tcase(s, tc_eval);

  return s;
}

int main(int argc, char **argv) {
  if (check_early_exit_setup() < 0) {
    fprintf(stderr, "Failed to setup early exit testing.\n");
    return EXIT_FAILURE;
  }

  Suite *s = eval_suite();
  SRunner *sr = srunner_create(s);

  check_init_logs();
  srunner_run_all(sr, CK_NORMAL);
  int number_failed = srunner_ntests_failed(sr);
  number_failed = check_early_exit(number_failed);
  check_print_logs();
  srunner_free(sr);

  return number_failed == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}
