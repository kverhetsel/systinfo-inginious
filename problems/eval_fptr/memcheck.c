#include "defs.h"

static void use_expression(expression *expr) {
  if (!expr) return;
  eval(expr);
  free_expression(expr);
}

int main(int argc, char **argv) {
  use_expression(constant(2));

  use_expression(constant(3.5));
  use_expression(add(constant(20.0), constant(0)));
  use_expression(divide(constant(1),
                         multiply(constant(1.41), constant(1.41))));
  use_expression(divide(constant(1),
                         add(constant(1.41), constant(1.41))));
  use_expression(subtract(constant(1),
                           add(constant(1.0), constant(2.0))));

  return 0;
}
