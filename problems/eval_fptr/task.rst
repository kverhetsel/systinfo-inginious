==================================================
 Représentation alternative d’une expression en C
==================================================

:author: Kilian Verhetsel
:environment: systinfo
:name: eval_fptr
:order: 12
:limit-time: 10
:limit-memory: 128
:limit-output: 5210

L’exercice `Représentaton d’une expression en C` vous propose de manipuler une
représentation pour une expression arithmétique en C. Un autre choix qu’on
aurait pu faire est celui-ci::

  typedef struct expression {
    float (*eval)(void *expr);
    void (*release)(void *expr);
  } expression;

Le pointeur de fonction ``eval`` est utilisé pour calculer la valeur de
l’expression et ``release`` pour libérer la mémoire qu’elle utilise.

Le but de la question est de comparer les solutions aux questions suivantes avec
celle de la question mentionnée auparavant afin de voir les avantages et
inconvénients d’une représentation sur l’autre.

Création d’une constante
========================

.. question:: constant
   :type: code
   :language: c

   Écrivez la fonction ``expression *constant(float n)``, ainsi que toute autre
   définition nécessaire. Cette fonction doit renvoyer une expression
   représentant une constante ou ``NULL`` en cas d’erreur.

Création d’une expression
=========================

.. question:: binop
   :type: code
   :language: c

   Écrivez les quatre fonctions suivantes (avec toutes les définitions
   nécessaires) :

   1. ``expression *add(expression *a, expression *b)``,
   2. ``expression *subtract(expression *a, expression *b)``,
   3. ``expression *multiply(expression *a, expression *b)``,
   4. et ``expression *divide(expression *a, expression *b)``.

   Ces fonctions sont utilisées pour obtenir la somme, la différence entre, le
   produit et le quotient de deux autres expressions. Par exemple,
   ``add(constant(3), divide(constant(4), constant(2)))`` représente ``3 +
   (4/2)``.

   Pour toutes les fonctions, si vous n’arrivez pas à allouer de la mémoire,
   vous devez renvoyer ``NULL``.

Libérer une expression
======================

.. question:: free_expression
   :type: code
   :language: c

   Écrivez le corps de la fonction ``void free_expression(expression *expr)``
   qui libère la mémoire utilisée par une expression.

Évaluer la valeur d’une expression
==================================

.. question:: eval
   :type: code
   :language: c

   Écrivez le corps de la fonction ``float eval(const expression *expr)`` qui
   doit renvoyer la valeur de l’expression pointée par ``expr``.
