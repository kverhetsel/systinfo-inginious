#ifndef DEFS_H_
#define DEFS_H_

typedef struct expression {
  enum {
    Addition, Subtraction, Multiplication, Division,
    Constant
  } type;

  union {
    struct {
      struct expression *lhs, *rhs;
    } binary_operator;

    float constant;
  } as;
} expression;

expression *constant(float n);

expression *add(expression *a, expression *b);
expression *subtract(expression *a, expression *b);
expression *multiply(expression *a, expression *b);
expression *divide(expression *a, expression *b);

float eval(const expression *expr);

void free_expression(expression *expr);

#endif
