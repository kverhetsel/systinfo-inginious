#include "defs.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "constant.h"

#include "binop.h"

void free_expression(expression *expr) {
#include "free_expression.h"
}

float eval(const expression *expr) {
#include "eval.h"
}
