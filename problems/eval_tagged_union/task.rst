======================================
 Représentation d’une expression en C
======================================

:author: Kilian Verhetsel
:environment: systinfo
:name: eval_tagged_union
:order: 11
:limit-time: 10
:limit-memory: 128
:limit-output: 5210

Supposons que l’on veuille écrire un programme pour calculer la valeur
d’expression arithmétiques en C (un exemple plus complexe où la même approche
pourrait être utilisée est pour celui d’un compilateur). On voudrait alors
choisir une représentation qui rend la manipulation de ces expressions plus
aisée qu’en cherchant l’information dans une chaîne de caractère directement. Un
choix possible en C est une structure telle que la suivante::

  typedef struct expression {
    enum {
      Addition, Subtraction, Multiplication, Division,
      Constant
    } type;

    union {
      struct {
        struct expression *lhs, *rhs;
      } binary_operator;

      float constant;
    } as;
  } expression;

Cette structure peut être utilisée pour représenter une expression
arithmétique simple : soit on a une constante et ``expression.as.constant``
contient sa valeur, soit on a un opérateur qui contient à sa gauche une
expression (``expression.as.binary_operator.lhs``) et une autre à sa droite
(``expression.as.binary_operator.rhs``).

Dans cet exercice, vous allez écrire des fonctions pour manipuler cette
structure.

Création d’une constante
========================

.. question:: constant
   :type: code
   :language: c

   Écrivez la fonction ``expression *constant(float n)``, ainsi que toute autre
   définition nécessaire. Cette fonction doit renvoyer une expression
   représentant une constante ou ``NULL`` en cas d’erreur.

Création d’une expression
=========================

.. question:: binop
   :type: code
   :language: c

   Écrivez les quatre fonctions suivantes (avec toutes les définitions
   nécessaires) :

   1. ``expression *add(expression *a, expression *b)``,
   2. ``expression *subtract(expression *a, expression *b)``,
   3. ``expression *multiply(expression *a, expression *b)``,
   4. et ``expression *divide(expression *a, expression *b)``.

   Ces fonctions sont utilisées pour obtenir la somme, la différence entre, le
   produit et le quotient de deux autres expressions. Par exemple,
   ``add(constant(3), divide(constant(4), constant(2)))`` représente ``3 +
   (4/2)``.

   Pour toutes les fonctions, si vous n’arrivez pas à allouer de la mémoire,
   vous devez renvoyer ``NULL``.

Libérer une expression
======================

.. question:: free_expression
   :type: code
   :language: c

   Écrivez le corps de la fonction ``void free_expression(expression *expr)``
   qui libère la mémoire utilisée par une expression.

Évaluer la valeur d’une expression
==================================

.. question:: eval
   :type: code
   :language: c

   Écrivez le corps de la fonction ``float eval(const expression *expr)`` qui
   doit renvoyer la valeur de l’expression pointée par ``expr``.
