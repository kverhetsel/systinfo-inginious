#include "image.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

uint32_t swap32(uint32_t n) {
#include "swap32.h"
}

int read_image(const char *filename, image_data *out) {
#include "read_image.h"
}

int write_image(const char *filename, const image_data *in) {
#include "write_image.h"
}
