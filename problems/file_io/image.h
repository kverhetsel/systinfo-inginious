#ifndef IMAGE_H_
#define IMAGE_H_

#include <stdint.h>

typedef struct color {
  uint8_t r, g, b;
} color;

typedef struct image_data {
  uint32_t width, height;
  color *data;
} image_data;

uint32_t swap32(uint32_t n);
int read_image(const char *filename, image_data *out);
int write_image(const char *filename, const image_data *in);

#endif
