#include <check.h>
#include <stdio.h>
#include <check_helpers.h>
#include <stdlib_hooks.h>
#include <stdlib.h>
#include "image.h"
#include "image_utils.h"

static
void compare_image(const image_data *expected, const image_data *actual) {
  ck_assert_msg(expected->width == actual->width,
                "La largeur de l’image est %zu mais aurait dû être %zu",
                expected->width, actual->width);
  ck_assert_msg(expected->height == actual->height,
                "La hauteur de l’image est %zu mais aurait dû être %zu",
                expected->height, actual->height);

  ck_assert_msg(actual->data != NULL,
                "Le pointeur vers les données de l’image est NULL");

  for (size_t y = 0; y < expected->height; y++) {
    for (size_t x = 0; x < expected->width; x++) {
      check_log("Accès à la couleur du point (x, y) = (%zu, %zu)", x, y);
      color a = expected->data[y*expected->width + x];
      color b = actual->data[y*expected->width + x];

      ck_assert_msg(a.r == b.r && a.g == b.g && a.b == b.b,
                    "La couleur au point (x, y) = (%zu, %zu) vaut "
                    "(r, g, b) = (%d, %d, %d) mais aurait dû être "
                    "(%d, %d, %d)", x, y,
                    b.r, b.g, b.b, a.r, a.g, a.b);
    }
  }
}

static
int correct_read_image(const char *filename, image_data *out,
                       uint32_t expected_width, uint32_t expected_height) {
  FILE *file = fopen(filename, "r");
  ck_assert_msg(file != NULL, "Le fichier n’a pas pu être ouvert.");

  uint32_t width, height;

  if (fread(&width, sizeof(width), 1, file) != 1) {
    ck_assert_msg(0, "La largeur de l’image n’a pas pu être lue");
  }

  if (fread(&height, sizeof(height), 1, file) != 1) {
    ck_assert_msg(0, "La hauteur de l’image n’a pas pu être lue");
  }

  width  = swap32(width);
  height = swap32(height);

  if (width != expected_width) {
    ck_assert_msg(swap32(width) != expected_width,
                  "Vous avez oublié de changer "
                  "l’endianness de la largeur.");
    ck_assert_msg(0, "La largeur est %lu mais aurait dû être %lu",
                  width, expected_width);
  }

  if (height != expected_height) {
    ck_assert_msg(swap32(height) != expected_height,
                  "Vous avez oublié de changer "
                  "l’endianness de la hauteur.");
    ck_assert_msg(0, "La hauteur est %lu mais aurait du être %lu",
                  height, expected_height);
  }

  color *buffer = malloc(sizeof(*buffer) * width * height);
  if (!buffer) goto fail_alloc;

  size_t n;
  if ((n = fread(buffer, sizeof(color), width*height, file)) != width*height) {
    ck_assert_msg(0, "Seuls %zu pixels ont pu être lus. Vérifiez que votre "
                  "programme écrit bien l’entierté du fichier.");
  }

  out->width = width;
  out->height = height;
  out->data = buffer;

  return 0;

fail_alloc: fclose(file);
            return -1;
}

NO_EARLY_EXIT_START_TEST(test_swap) {
  check_log("Appel à swap32");
  ck_assert_int_eq(swap32(0x00000000u), 0x00000000u);
  ck_assert_int_eq(swap32(0xFF0000FFu), 0xFF0000FFu);
  ck_assert_int_eq(swap32(0x000000FFu), 0xFF000000u);
  ck_assert_int_eq(swap32(0x0000FF00u), 0x00FF0000u);
  ck_assert_int_eq(swap32(0x00FF0000u), 0x0000FF00u);
  ck_assert_int_eq(swap32(0xFF000000u), 0x000000FFu);
  ck_assert_int_eq(swap32(0xAABBCCDDu), 0xDDCCBBAAu);
  ck_assert_int_eq(swap32(0x01234567u), 0x67452301u);
  ck_assert_int_eq(swap32(0x00122100u), 0x00211200u);
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_read_image) {
  for (size_t i = 0; i < 10; i++) {
    image_data expected, actual;
    random_image(&expected);

    fake_write_image("test.img", &expected, expected.width, expected.height);
    check_log("Appel à read_image");

    /* Make errors more obvious */
    actual.width = actual.height = 0;
    actual.data = NULL;

    ck_assert_msg(read_image("test.img", &actual) == 0,
                  "read_image signale une erreur pour un appel valide.");
    compare_image(&expected, &actual);

    release_image(&expected);
    check_log("Libération de l’image renvoyée par read_image");
    release_image(&actual);
  }
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_read_image_too_short) {
  image_data expected, actual;
  random_image(&expected);

  fake_write_image("test.img", &expected, expected.width/2,
                   expected.height/2);

  check_log("Appel à read_image (le fichier d’entrée s’arrête prématurément)");
  ck_assert_msg(read_image("test.img", &actual) < 0,
                "read_image ne signale pas d’erreur alors que les données "
                "s’arrêtent prématurément");
  release_image(&expected);
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_read_image_no_header) {
  struct {
    uint32_t width, height;
  } header = {50, 38};

  for (size_t i = 0; i < sizeof(header); i++) {
    image_data actual;

    FILE *f = fopen("test.img", "w");
    fwrite(&header, 1, i, f);
    fclose(f);

    check_log("Appel à read_image (le fichier d’entrée s’arrête "
              "prématurément)");
    ck_assert_msg(read_image("test.img", &actual) < 0,
                  "read_image ne signale pas d’erreur alors que le fichier "
                  "s’arrête prématurément");
  }
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_read_image_no_file) {
  image_data actual;
  check_log("Appel à read_image (le fichier d’entrée n’existe pas)");
  ck_assert_msg(read_image("this_does_not_exist.img", &actual) < 0,
                "read_image ne signale pas d’erreur alors que le fichier  "
                "d’entrée n’existe pas.");
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_read_image_no_alloc) {
  for (size_t i = 0; i < 10; i++) {
    image_data expected, actual;
    random_image(&expected);

    fake_write_image("test.img", &expected, expected.width, expected.height);

    /* Make errors more obvious */
    actual.width = actual.height = 0;
    actual.data = NULL;

    check_log("Appel à read_image quand malloc va échouer");
    hook_alloc_force_fail();
    int res = read_image("test.img", &actual);
    hook_alloc_stop_fail();

    ck_assert_msg(res < 0,
                  "Ne signale pas d’erreur alors que malloc a échoué.");
    release_image(&expected);
  }
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_write_image) {
  for (size_t i = 0; i < 10; i++) {
    image_data expected, actual;

    actual.width = actual.height = 0;
    actual.data = NULL;

    random_image(&expected);

    uint32_t w = expected.width;
    uint32_t h = expected.height;

    fake_write_image("comparison.img", &expected, w, h);
    check_log("Appel à write_imagee");
    ck_assert_msg(write_image("test.img", &expected) == 0,
                  "Un appel valide à write_image a échoué");
    check_log("Libération de l’image pasée à write_image");
    release_image(&expected);

    correct_read_image("comparison.img", &expected, w, h);
    correct_read_image("test.img", &actual, w, h);
    compare_image(&expected, &actual);
    release_image(&expected);
    release_image(&actual);
  }
} NO_EARLY_EXIT_END_TEST

Suite *image_suite(void) {
  Suite *s = suite_create("Image");

  TCase *tc_swap = tcase_create("swap32");
  tcase_add_logging_test(tc_swap, test_swap);
  suite_add_tcase(s, tc_swap);

  TCase *tc_read = tcase_create("read_image");
  tcase_add_logging_test(tc_read, test_read_image);
  tcase_add_logging_test(tc_read, test_read_image_too_short);
  tcase_add_logging_test(tc_read, test_read_image_no_header);
  tcase_add_logging_test(tc_read, test_read_image_no_file);
  tcase_add_logging_test(tc_read, test_read_image_no_alloc);
  suite_add_tcase(s, tc_read);

  TCase *tc_write = tcase_create("write_image");
  tcase_add_logging_test(tc_write, test_write_image);
  suite_add_tcase(s, tc_write);

  return s;
}

int main(int argc, char **argv) {
  if (check_early_exit_setup() < 0) {
    fprintf(stderr, "Failed to setup early exit testing.\n");
    return EXIT_FAILURE;
  }

  srand(0);

  Suite *s = image_suite();
  SRunner *sr = srunner_create(s);

  check_init_logs();
  srunner_run_all(sr, CK_NORMAL);
  int number_failed = srunner_ntests_failed(sr);
  number_failed = check_early_exit(number_failed);
  check_print_logs();
  srunner_free(sr);

  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
