#ifndef IMAGE_UTILS_H_
#define IMAGE_UTILS_H_

#include "image.h"
#include <stdlib.h>
#include <stdio.h>

static
void random_image(image_data *img) {
  img->width  = 1 + (rand() % 100);
  img->height = 1 + (rand() % 100);

  img->data = malloc(sizeof(*img->data) * img->width * img->height);
  for (size_t i = 0; i < img->width*img->height; i++) {
    img->data[i].r = rand() % 256;
    img->data[i].g = rand() % 256;
    img->data[i].b = rand() % 256;
  }
}

static
void release_image(image_data *img) {
  free(img->data);
}

static
int fake_write_image(const char *filename, const image_data *in,
                     size_t write_width, size_t write_height) {
  FILE *file = fopen(filename, "w");
  if (!file) return -1;

  int status = 0;

  uint32_t width  = swap32(in->width);
  uint32_t height = swap32(in->height);

  if (fwrite(&width, sizeof(width), 1, file) != 1) {
    status = -1;
    goto fail_write;
  }

  if (fwrite(&height, sizeof(height), 1, file) != 1) {
    status = -1;
    goto fail_write;
  }

  size_t to_write = write_width * write_height;
  if (fwrite(in->data, sizeof(color), to_write, file) != to_write) {
    status = -1;
    goto  fail_write;
  }

fail_write: fclose(file);
            return status;
}

#endif
