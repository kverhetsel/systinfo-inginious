#include <stdlib.h>
#include <stdlib_hooks.h>
#include "image.h"
#include "image_utils.h"

int main(int argc, char **argv) {
  for (size_t i = 0; i < 100; i++) {
    image_data img;
    random_image(&img);
    fake_write_image("foo", &img, img.width, img.height);
    release_image(&img);

    if (read_image("foo", &img) == 0)
      release_image(&img);
  }

  for (size_t i = 0; i < 100; i++) {
    image_data img;
    random_image(&img);
    fake_write_image("foo", &img, img.width/2, img.height/2);
    release_image(&img);

    if (read_image("foo", &img) == 0)
      release_image(&img);
  }

  for (size_t i = 0; i < 100; i++) {
    image_data img;
    random_image(&img);
    write_image("bar", &img);
    release_image(&img);
  }

  image_data img;
  if (read_image("qux", &img) == 0) release_image(&img);

  return 0;
}
