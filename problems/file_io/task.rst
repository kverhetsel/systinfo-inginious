==================================
 Manipulation d’un format binaire
==================================

:author: Kilian Verhetsel
:environment: systinfo
:name: file_io
:order: 5
:limit-time: 10
:limit-memory: 128
:limit-output: 5210

Dans cet exercice, vous allez manipuler un fichier en C afin de lire des données
et d’en écrire.

Le but est d’utiliser le disque dur pour stocker des images représentées par la
structure ``image_data`` avec la définition suivante::

  typedef struct color {
    uint8_t r, g, b;
  } color;

  typedef struct image_data {
    uint32_t width, height;
    color *data;
  } image_data;

Ces informations seront contenues dans un format binaire (très simplifié par
rapport aux formats d’images réels) qui sera décrit plus bas.

Endianness
==========

.. question:: swap32
   :type: code
   :language: c

   Lire un fichier dans un format binaire est assez facile en C parce qu’il y a une
   correspondance plus ou moins directe entre le contenu du fichier et le contenu
   d’une structure en C. Un problème de portabilité survient néanmoins quand on
   veut manipuler des entiers : l’endianness (cfr. le cours). Beaucoup de formats
   et de protocoles sont spécifiés en big-endian, mais par exemple, l’architecture
   x86 utilise le format little-endian. Il faut donc être capable de passer d’une
   représentation à l’autre.

   Écrivez le corps de la fonction ``uint32_t swap32(uint32_t n)`` qui inverse
   l’ordre des bytes de ``n`` et renvoie cette nouvelle valeur (cette fonction peut
   donc être utilisée pour effectuer la conversion dans les deux sens). Comme le
   type l’indique, l’argument en entrée est un entier 32 bits non-signé.

Lecture des données
===================

.. question:: read_image
   :type: code
   :language: c

   Le format d’image utilisé dans cette fonction contient les champs suivants,
   dans l’ordre :

   - un entier 32 bits non-signé en big-endian pour la largeur de l’image
     (``width``) ;
   - un entier 32 bits non-signé en big-endian pour la hauteur de l’image
     (``height``) ;
   - la couleur de chacun des ``width * height`` points de l’image (avec dans
     l’ordre tous les points de la première ligne, suivis de tous les points
     de la seconde, et ainsi de suite).

   Chaque couleur est représentée comme trois entiers 8 bits non-signés,
   qui représentent dans l’ordre les composantes rouge, verte et bleue de la
   couleur du point.

   Dans cette question et la suivante, vous pouvez supposer que le processeur
   fonctionne en little-endian et que vous avec donc besoin de la fonction
   ``swap32``.

   Écrivez le corps de la fonction ``int read_image(const char *filename,
   image_data *out)`` qui doit ouvrir le fichier dont le chemin est contenu par
   ``filename``, et en lire le contenu pour remplir la structure pointée par
   ``out``. Vous devez vous-mêmes allouer la mémoire pour ``out->data`` et y
   stocker la couleur de tous les points dans le même ordre que dans le
   fichier.

   Vous devez renvoyer 0 si tout s’est bien passé ou un entier négatif en cas
   d’erreur.

Écriture de données
===================

.. question:: write_image
   :type: code
   :language: c

   Écrivez le corps de la fonction ``int write_image(const char *filename,
   const image_data *in)`` qui doit ouvrir le fichier dont le chemin est contenu
   par ``filename`` et y écrire le contenu de l’image pointée par ``in`` en
   utilisant le format décrit dans la question précédente.

   Si le fichier existe déjà, vous devez remplacer son contenu par celui de
   l’image passée en argument.

   Vous devez renvoyer 0 si tout s’est bien passé ou un entier négatif en cas
   d’erreur.
