#ifndef DEFS_H_
#define DEFS_H_

#include <stddef.h>

typedef enum cell { live, dead } cell;
void game_of_life(cell *grid, cell *new_grid, size_t width, size_t height);

#endif
