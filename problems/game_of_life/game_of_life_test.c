#include "defs.h"
#include <string.h>
#include <check_helpers.h>
#include <stdlib.h>
#include <stdio.h>

#define X live
#define O dead

static size_t block_w = 4;
static size_t block_h = 4;
static cell block[][4*4] = {
  {O, O, O, O,
   O, X, X, O,
   O, X, X, O,
   O, O, O, O},
  {O, O, O, O,
   O, X, X, O,
   O, X, X, O,
   O, O, O, O},
  {O, O, O, O,
   O, X, X, O,
   O, X, X, O,
   O, O, O, O},
};

static size_t beehive_w = 6;
static size_t beehive_h = 5;
static cell beehive[][6*5] = {
  {O, O, O, O, O, O,
   O, O, X, X, O, O,
   O, X, O, O, X, O,
   O, O, X, X, O, O,
   O, O, O, O, O, O},
  {O, O, O, O, O, O,
   O, O, X, X, O, O,
   O, X, O, O, X, O,
   O, O, X, X, O, O,
   O, O, O, O, O, O},
  {O, O, O, O, O, O,
   O, O, X, X, O, O,
   O, X, O, O, X, O,
   O, O, X, X, O, O,
   O, O, O, O, O, O},
};

static size_t loaf_w = 6;
static size_t loaf_h = 6;
static cell loaf[][6*6] = {
  {O, O, O, O, O, O,
   O, O, X, X, O, O,
   O, X, O, O, X, O,
   O, O, X, O, X, O,
   O, O, O, X, O, O,
   O, O, O, O, O, O},
  {O, O, O, O, O, O,
   O, O, X, X, O, O,
   O, X, O, O, X, O,
   O, O, X, O, X, O,
   O, O, O, X, O, O,
   O, O, O, O, O, O},
  {O, O, O, O, O, O,
   O, O, X, X, O, O,
   O, X, O, O, X, O,
   O, O, X, O, X, O,
   O, O, O, X, O, O,
   O, O, O, O, O, O},
};

static size_t boat_w = 5;
static size_t boat_h = 5;
static cell boat[][5*5] = {
  {O, O, O, O, O,
   O, X, X, O, O,
   O, X, O, X, O,
   O, O, X, O, O,
   O, O, O, O, O,},
};

static size_t blinker_w = 5;
static size_t blinker_h = 5;
static cell blinker[][5*5] = {
  {O, O, O, O, O,
   O, O, X, O, O,
   O, O, X, O, O,
   O, O, X, O, O,
   O, O, O, O, O,},
  {O, O, O, O, O,
   O, O, O, O, O,
   O, X, X, X, O,
   O, O, O, O, O,
   O, O, O, O, O,},
  {O, O, O, O, O,
   O, O, X, O, O,
   O, O, X, O, O,
   O, O, X, O, O,
   O, O, O, O, O,},
  {O, O, O, O, O,
   O, O, O, O, O,
   O, X, X, X, O,
   O, O, O, O, O,
   O, O, O, O, O,},
  {O, O, O, O, O,
   O, O, X, O, O,
   O, O, X, O, O,
   O, O, X, O, O,
   O, O, O, O, O,},
};

static size_t toad_w = 6;
static size_t toad_h = 6;
static cell toad[][6*6] = {
  {O, O, O, O, O, O,
   O, O, O, O, O, O,
   O, O, X, X, X, O,
   O, X, X, X, O, O,
   O, O, O, O, O, O,
   O, O, O, O, O, O},
  {O, O, O, O, O, O,
   O, O, O, X, O, O,
   O, X, O, O, X, O,
   O, X, O, O, X, O,
   O, O, X, O, O, O,
   O, O, O, O, O, O},
  {O, O, O, O, O, O,
   O, O, O, O, O, O,
   O, O, X, X, X, O,
   O, X, X, X, O, O,
   O, O, O, O, O, O,
   O, O, O, O, O, O},
  {O, O, O, O, O, O,
   O, O, O, X, O, O,
   O, X, O, O, X, O,
   O, X, O, O, X, O,
   O, O, X, O, O, O,
   O, O, O, O, O, O},
  {O, O, O, O, O, O,
   O, O, O, O, O, O,
   O, O, X, X, X, O,
   O, X, X, X, O, O,
   O, O, O, O, O, O,
   O, O, O, O, O, O},
  {O, O, O, O, O, O,
   O, O, O, X, O, O,
   O, X, O, O, X, O,
   O, X, O, O, X, O,
   O, O, X, O, O, O,
   O, O, O, O, O, O},
};

static size_t beacon_w = 6;
static size_t beacon_h = 6;
static cell beacon[][6*6] = {
  {O, O, O, O, O, O,
   O, X, X, O, O, O,
   O, X, X, O, O, O,
   O, O, O, X, X, O,
   O, O, O, X, X, O,
   O, O, O, O, O, O},
  {O, O, O, O, O, O,
   O, X, X, O, O, O,
   O, X, O, O, O, O,
   O, O, O, O, X, O,
   O, O, O, X, X, O,
   O, O, O, O, O, O},
  {O, O, O, O, O, O,
   O, X, X, O, O, O,
   O, X, X, O, O, O,
   O, O, O, X, X, O,
   O, O, O, X, X, O,
   O, O, O, O, O, O},
  {O, O, O, O, O, O,
   O, X, X, O, O, O,
   O, X, O, O, O, O,
   O, O, O, O, X, O,
   O, O, O, X, X, O,
   O, O, O, O, O, O},
  {O, O, O, O, O, O,
   O, X, X, O, O, O,
   O, X, X, O, O, O,
   O, O, O, X, X, O,
   O, O, O, X, X, O,
   O, O, O, O, O, O},
  {O, O, O, O, O, O,
   O, X, X, O, O, O,
   O, X, O, O, O, O,
   O, O, O, O, X, O,
   O, O, O, X, X, O,
   O, O, O, O, O, O},
};

static size_t glider_w = 10;
static size_t glider_h = 10;
static cell glider[][10*10] = {
  {O, O, O, O, O, O, O, O, O, O,
   O, O, X, O, O, O, O, O, O, O,
   O, O, O, X, O, O, O, O, O, O,
   O, X, X, X, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O},
  {O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, X, O, X, O, O, O, O, O, O,
   O, O, X, X, O, O, O, O, O, O,
   O, O, X, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O},
  {O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, X, O, O, O, O, O, O,
   O, X, O, X, O, O, O, O, O, O,
   O, O, X, X, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O},
  {O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, X, O, O, O, O, O, O, O,
   O, O, O, X, X, O, O, O, O, O,
   O, O, X, X, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O},
  {O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, X, O, O, O, O, O, O,
   O, O, O, O, X, O, O, O, O, O,
   O, O, X, X, X, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O,
   O, O, O, O, O, O, O, O, O, O}
};

static size_t blinker_on_border_w = 3;
static size_t blinker_on_border_h = 3;
static cell blinker_on_border[][3*3] = {
  {O, X, O,
   O, X, O,
   O, X, O},
  {O, O, O,
   X, X, X,
   O, O, O},
  {O, X, O,
   O, X, O,
   O, X, O},
  {O, O, O,
   X, X, X,
   O, O, O},
  {O, X, O,
   O, X, O,
   O, X, O},
};

#undef X
#undef O

#define array_length(ary) (sizeof(ary)/sizeof(*(ary)))

size_t count_neighbors(cell *grid, size_t width, size_t height,
                       size_t x, size_t y) {
  size_t count = 0;
  for (int dy = -1; dy <= 1; dy++) {
    if (dy == -1 && y == 0) continue;
    if (dy == 1 && y == height-1) continue;

    for (int dx = -1; dx <= 1; dx++) {
      if (dx == -1 && x == 0) continue;
      if (dx == 1 && x == width-1) continue;
      if (dx == 0 && dy == 0) continue;

      if (grid[(x+dx)+(y+dy)*width] == live) count++;
    }
  }

  return count;
}

const char *state_name(cell c) {
  switch (c) {
  case live: return "vivante";
  case dead: return "morte";
  default: return "dans un état invalide";
  }
}

void try_pattern(cell *input, cell *expected, cell *result,
                 size_t width, size_t height) {
  cell copy[width*height];
  memcpy(copy, input, sizeof(copy));

  check_log("Appel à game_of_life avec une grille de taille %zux%zu",
            width, height);
  game_of_life(input, result, width, height);

  check_log("Comparaison de l’entrée avec sa valeur originale");
  ck_assert_msg(memcmp(input, copy, sizeof(copy)) == 0,
                "Votre programme modifie la grille d’entrée.");
  for (size_t y = 0; y < height; y++) {
    for (size_t x = 0; x < width; x++) {
      check_log("Accès à la case (%zu, %zu) d’une grille de taille %zux%zu",
                x, y, width, height);

      size_t i = x + y * width;
      size_t neighbors = count_neighbors(input, width, height, x, y);

      ck_assert_msg(result[i] == expected[i],
                    "Votre programme ne respecte pas les règles : "
                    "une cellule %s avec %zu voisin%s en vie est maintenant %s",
                    state_name(input[i]), neighbors, neighbors < 2 ? "" : "s",
                    state_name(result[i]));
    }
  }
}

#define try_pattern_series(name)                                \
  do {                                                          \
    cell state[array_length(name[0])];                          \
    cell copy[array_length(name[0])];                           \
    memcpy(state, name[0], sizeof(state));                      \
                                                                \
    for (size_t t = 1; t < array_length(name); t++) {           \
      try_pattern(state, name[t], copy, name##_w, name##_h);    \
      memcpy(state, copy, sizeof(state));                       \
    }                                                           \
  } while (0)

NO_EARLY_EXIT_START_TEST(test_patterns) {
  try_pattern_series(block);
  try_pattern_series(beehive);
  try_pattern_series(loaf);
  try_pattern_series(boat);
  try_pattern_series(blinker);
  try_pattern_series(toad);
  try_pattern_series(beacon);
  try_pattern_series(glider);
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_border) {
  try_pattern_series(blinker_on_border);
} NO_EARLY_EXIT_END_TEST

#undef try_pattern_series

Suite *game_of_life_suite(void) {
  Suite *s = suite_create("Game of life");

  TCase *tc_core = tcase_create("Core");
  tcase_add_logging_test(tc_core, test_patterns);
  tcase_add_logging_test(tc_core, test_border);
  suite_add_tcase(s, tc_core);

  return s;
}

int main(int argc, char **argv) {
  if (check_early_exit_setup() < 0) {
    fprintf(stderr, "Failed to setup early exit testing.\n");
    return EXIT_FAILURE;
  }

  Suite *s = game_of_life_suite();
  SRunner *sr = srunner_create(s);

  check_init_logs();
  srunner_run_all(sr, CK_NORMAL);
  int number_failed = srunner_ntests_failed(sr);
  number_failed = check_early_exit(number_failed);
  check_print_logs();
  srunner_free(sr);

  return number_failed == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}
