#include "defs.h"
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv) {
  for (size_t i = 0; i < 100; i++) {
    size_t w = 1 + rand() % 100;
    size_t h = 1 + rand() % 100;
    cell grid[w*h];
    cell result[w*h];
    for (size_t i = 0; i < w*h; i++) grid[i] = rand() % 2 == 0 ? live : dead;

    for (size_t i = 0; i < 10; i++) {
      game_of_life(grid, result, w, h);
      memcpy(grid, result, sizeof(grid));
    }
  }

  return 0;
}
