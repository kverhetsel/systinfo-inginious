==================
 Le jeu de la vie
==================

:author: Kilian Verhetsel
:environment: systinfo
:name: game_of_life
:order: 10
:limit-time: 10
:limit-memory: 128
:limit-output: 5210

Le jeu de la vie est un automate cellulaire sur une grille deux
dimensionnelles. Chaque cellule est soit vivante, soit morte. À chaque étape, le
contenu de la grille est mis à jour en suivant les règles suivantes :

1. si une cellule vivante a mois de 2 voisins en vie, elle meurt ;
2. si une cellule vivante a 2 ou 3 voisins en vie, elle reste en vie ;
3. si une cellule vivante a plus de 3 voisins en vie, elle meurt ;
4. si une cellule morte a exactement 3 voisins en vie, elle naît.

Le nombre de voisins en vie est déterminé en regardant les cellules à gauche, à
droite, en haut, en bas, et sur les diagonales (chaque cellule à donc 8 autres
cellules voisines).

En principe, l’automate devrait s’exécuter sur une grille infinie. Pour
simplifier le problème, dans cette question, il suffit d’implémenter le jeu de
la vie sur une grille de taille finie (donc les cellules sur les bords ont moins
de voisin).

Dans cette question, les cellules sont représentées par l’énumération suivante::

  typedef enum cell { live, dead } cell;

Le jeu de la vie
================

.. question:: game_of_life
   :type: code
   :language: c

   Écrivez le code de la fonction ``void game_of_life(cell *grid, cell
   *new_grid, size_t width, size_t height)`` qui doit appliquer les règles
   citées précédemment et stocker la grille mise à jour dans ``new_grid`` (sans
   changer ``grid``). La hauteur de la grille est ``height`` et sa largeur est
   ``width``. L’indice de l’élément dont les coordonnées sont ``(x, y)`` est
   ``x + y*width``.
