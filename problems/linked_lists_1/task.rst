====
List
====

:author: Kilian Verhetsel
:environment: systinfo
:name: list
:order: 1
:limit-time: 10
:limit-memory: 128
:limit-output: 5210

Une liste chaînée est une structure de données permettant de représenter une
séquence d’éléments. Dans cet exercice, une liste chaînée sera représentée par
un pointeur sur la structure suivante::

     typedef struct node {
       int value; /* valeur du nœud */
       struct node *next; /* pointeur vers l’élément suivant */
     } node;

La liste vide est représentée par un pointeur nul.

Le but des questions suivantes est de vous familiariser avec les listes chaînées
en C.

(Remarque : vous ne devez jamais traiter le cas des listes contenant un cycle.)

Longueur d’une liste chaînée
============================

.. question:: length
   :type: code
   :language: c

   Écrivez le corps de la fonction ``size_t length(node *list)``, qui doit
   renvoyer le nombre d’éléments se trouvant dans la liste.


Compter les éléments d’une liste chaînée
========================================

.. question:: count
   :type: code
   :language: c

   Écrivez le corps de la fonction ``size_t count(node *list, int value)``, qui
   doit renvoyer le nombre d’éléments dont la valeur est ``value`` se trouvant
   dans la liste.

Rajouter un élément
===================

.. question:: push
   :type: code
   :language: c

   Écrivez le corps de la fonction ``int push(node **list, int value)``,
   permettant de rajouter un élément au début d’une liste.

   Le premier argument est un pointeur sur une liste. La fonction doit remplacer
   la liste pointée par une nouvelle liste où a été rajouté un élément.

   La fonction doit renvoyer ``0`` si elle réussit et une valeur négative si
   elle n’arrive pas à allouer de la mémoire.

   .. TODO: Insert push.png here

Supprimer un élément
====================

.. question:: pop
   :type: code
   :language: c

   Écrivez le corps de la fonction ``int pop(node **list)``, permettant de
   supprimer le premier élément d’une liste et d’en renvoyer la valeur.

   Le premier argument est un pointeur sur une liste. Vous pouvez supposer que
   cette liste ne sera jamais vide. La fonction doit remplacer la liste pointée
   par une liste où le premier élément a été supprimé.

Libérer une liste chaînée
=========================

.. question:: free_list
   :type: code
   :language: c

   Écrivez le corps de la fonction ``void free_list(node *list)``, permettant de
   libérer tous les nœuds d’une liste.

   .. TODO: Insert pop.png here
