=========================
 malloc, realloc et free
=========================

:author: Kilian Verhetsel
:environment: systinfo
:name: malloc
:order: 6
:limit-time: 10
:limit-memory: 128
:limit-output: 5210

``malloc``, ``realloc`` et ``free`` sont trois fonctions très courantes dans les
programmes écrits en C, et mal les utiliser est la source d’une grande partie
des erreurs faites quand on programme en C.

Afin de démystifier l’effet de ces fonctions, cet exercice vous propose d’écrire
un algorithme qui pourrait être utilisé dans une véritable
implémentation de ``malloc`` (``malloc`` n’est pas un appel système ; c’est une
fonction tout à fait normale qui utilise des appels systèmes comme ``sbrk`` et
``mmap`` pour obtenir de la mémoire).

Les définitions suivantes se trouvent dans un autre fichier::

  void *heap;
  size_t heap_size;

La tâche de votre implémentation consiste à distribuer lors des appels à vos
fonctions des segments de la zone de mémoire qui commence à l’adresse ``heap``
et se termine à ``(char*)heap + heap_size`` (non-inclus).

Vous devez implémenter les 3 fonctions suivantes :

- ``my_malloc`` qui se comporte comme ``malloc`` ;
- ``my_realloc`` qui se comporte comme ``realloc`` ;
- ``my_free`` qui se comporte comme ``free``.

Si vous avez des doutes sur le comportement d’une de ces fonctions, consultez sa
page de manuel. N’oubliez par exemple pas de gérer les erreurs si jamais il n’y
a plus assez de mémoire disponible.

Avant de commencer, réfléchissez :

- Où va se trouver l’information nécessaire pour trouver une zone de la mémoire
  qui n’est pas déjà utilisée ?
- Quand ``my_free`` est appelé, comment allez-vous savoir quelle quantité de
  mémoire est à nouveau disponible ?
- Quel genre de structure de données allez-vous employer dans votre
  implémentation ?

Implémentation
===============

.. question:: my_malloc
   :type: code
   :language: c

   Écrivez le contenu du fichier ``malloc.c`` qui contiendra les trois fonctions
   demandées ainsi que toutes les structure de données et fonctions auxiliaires
   dont vous aurez besoin.
