#include <stddef.h>

typedef struct meeting_point meeting_point;

meeting_point *make_meeting_point(void);
void free_meeting_point(meeting_point *point);
void meet(meeting_point *point, size_t n);
