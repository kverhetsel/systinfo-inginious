#include "meeting_point.h"
#include <stdlib_hooks.h>
#include <check_helpers.h>
#include <unistd.h>
#include <pthread.h>

NO_EARLY_EXIT_START_TEST(test_one_meet) {
  check_log("Appel à make_meeting_point");
  meeting_point *point = make_meeting_point();
  ck_assert_msg(point != NULL, "make_meeting_point renvoie NULL alors "
                "qu’aucune erreur n’aurait dû se produire.");

  /* tests do timeout eventually */
  check_log("Appel à meet (avec un seul thread)");
  meet(point, 1);

  check_log("Appel à free_meeting_point");
  free_meeting_point(point);
} NO_EARLY_EXIT_END_TEST

#define N 15
static struct thread_data {
  int before, after;
} global_data[N];

meeting_point *point;

void *thread_function(void *data) {
  struct thread_data *arg = data;

  arg->before = 1;
  usleep(1000*(arg - global_data));
  meet(point, N+1);
  arg->after = 1;

  return NULL;
}

NO_EARLY_EXIT_START_TEST(test_many_meet) {
  check_log("Appel à make_meeting_point");
  point = make_meeting_point();
  ck_assert_msg(point != NULL, "make_meeting_point renvoie NULL alors "
                "qu’aucune erreur n’aurait dû se produire.");

  pthread_t threads[N];
  for (size_t i = 0; i < N; i++)
    pthread_create(threads + i, NULL, thread_function, global_data + i);

  usleep(100*1000); /* quite enough to reach the meeting point */

  for (size_t i = 0; i < N; i++) {
    ck_assert(global_data[i].before);
    ck_assert_msg(!global_data[i].after, "Un des threads a dépassé le point de "
                  " rendez-vous trop tôt.");
  }

  check_log("Dernier appel à meet");
  meet(point, N+1);

  check_log("Attente de la fin de l’exécution des threads");
  for (size_t i = 0; i < N; i++) pthread_join(threads[i], NULL);

  for (size_t i = 0; i < N; i++) {
    ck_assert(global_data[i].before);
    ck_assert_msg(global_data[i].after, "Un des threads n’a pas passé "
                  " le point de rendez vous.");
  }

  check_log("Appel à free_meeting_point");
  free_meeting_point(point);
} NO_EARLY_EXIT_END_TEST

Suite *meeting_point_suite(void) {
  Suite *s = suite_create("Point de rendez-vous");

  TCase *tc_core = tcase_create("Core");
  tcase_add_logging_test(tc_core, test_one_meet);
  tcase_add_logging_test(tc_core, test_many_meet);
  suite_add_tcase(s, tc_core);

  return s;
}

int main(int argc, char **argv) {
  if (check_early_exit_setup() < 0) {
    fprintf(stderr, "Failed to setup early exit testing.\n");
    return EXIT_FAILURE;
  }

  srand(0);

  Suite *s = meeting_point_suite();
  SRunner *sr = srunner_create(s);

  check_init_logs();
  srunner_run_all(sr, CK_NORMAL);
  int number_failed = srunner_ntests_failed(sr);
  number_failed = check_early_exit(number_failed);
  check_print_logs();
  srunner_free(sr);

  return number_failed == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}
