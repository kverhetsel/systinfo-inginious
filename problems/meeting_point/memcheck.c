#include "meeting_point.h"
#include <pthread.h>
#include <unistd.h>

#define N 3

meeting_point *point;

void *thread_function(void *data) {
  meet(point, N+1);
  return NULL;
}

int main(int argc, char **argv) {
  point = make_meeting_point();

  pthread_t threads[N];
  for (size_t i = 0; i < N; i++)
    pthread_create(threads + i, NULL, thread_function, NULL);

  usleep(10*1000);
  meet(point, N+1);

  for (size_t i = 0; i < N; i++) pthread_join(threads[i], NULL);
  free_meeting_point(point);

  return 0;
}
