=========================
 Problème du rendez-vous
=========================

:author: Kilian Verhetsel
:environment: systinfo
:name: meeting_point
:order: 14
:limit-time: 10
:limit-memory: 128
:limit-output: 5210

Le problème du rendez-vous consiste à faire en sorte que plusieurs threads
travaillant en groupe puissent attendre que tous les threads aient terminés une
action avant de continuer leur exécution. Cet exercice vous demande d’utiliser
les sémaphores pour écrire une série de fonction pouvant être utilisées pour
résoudre le problèmes.

Solution
========

.. question:: meeting_point
   :type: code
   :language: c

   Il vous est demandé de définir les fonctions suivantes :

   1. ``struct meeting_point *make_meeting_point(void)`` qui alloue la mémoire
      et crée toutes les ressources dont votre implémentation a besoin pour
      fonctionner et renvoie ``NULL`` en cas d’erreur ;
   2. ``void free_meeting_point(struct meeting_point *point)`` qui libère ces
      ressources ;
   3. ``void meet(struct meeting_point *point, size_t n)`` est utilisé au point
      de rendez-vous. Cette fonction doit bloquer l’exécution jusqu’à ce que
      ``n`` threads aient atteint le point de rendez-vous.

   À vous de choisir le contenu de la structure ``meeting_point``.
