#include "shared_memory.h"

int main(int argc, char **argv) {
  srand(0);

  for (size_t i = 0; i < 100; i++) {
    size_t size = rand() % 1000;
    int *data = create_buffer(size*sizeof(int));
    for (size_t i = 0; i < size; i++) data[i] = 3;
    free_buffer(data);
  }

  return 0;
}
