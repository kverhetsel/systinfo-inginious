#include <stddef.h>
#include <stdlib.h>
#include <string.h>

void *create_buffer(size_t size);
void free_buffer(void *buffer);
void square(int *buffer, size_t start, size_t length);
