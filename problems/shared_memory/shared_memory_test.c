#include <stdlib.h>
#include <stdlib_hooks.h>
#include <check.h>
#include <check_helpers.h>
#include <string.h>
#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>

#include "shared_memory.h"

NO_EARLY_EXIT_START_TEST(test_create) {
  check_log("Appel à create buffer");
  int *data = create_buffer(100*sizeof(int));
  ck_assert_msg(data != NULL && data != (void*)-1,
                "create_buffer ne devrait pas renvoyer un pointeur invalide.");

  for (size_t i = 0; i < 100; i++) {
    check_log("Lecture à l’indice %zu d’un tableau de taille %d",
              i, 100);
    ck_assert_msg(data[i] == 0,
                  "L’élément à l’indice %zu vaut %d mais devrait être nul",
                  i, data[i]);
  }

  check_log("Appel à free_buffer");
  free_buffer(data);
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_square) {
  check_log("Appel à create buffer");
  int *data = create_buffer(100*sizeof(int));
  ck_assert_msg(data != NULL && data != (void*)-1,
                "create_buffer ne devrait pas renvoyer un pointeur invalide.");

  for (size_t i = 0; i < 100; i++) {
    check_log("Écriture à l’indice %zu d’un tableau de taille %d",
              i, 100);
    data[i] = i;
  }

  check_log("Appel à square");
  square(data, 10, 80);
  for (size_t i = 0; i < 10; i++) {
    check_log("Lecure à l’indice %zu d’un tableau de taille %d",
              i, 100);
    ck_assert_msg(data[i] == (int)i,
                  "square modifie des éléments avant le début de l’intervalle"
                  " demandé");
  }
  for (size_t i = 10; i < 90; i++) {
    check_log("Lecure à l’indice %zu d’un tableau de taille %d",
              i, 100);
    ck_assert_msg(data[i] == (int)(i*i),
                  "L’élément à l’indice %zu vaut %d mais aurait dû être %zu",
                  i, data[i], i*i);
  }
  for (size_t i = 90; i < 100; i++) {
    check_log("Lecure à l’indice %zu d’un tableau de taille %d",
              i, 100);
    ck_assert_msg(data[i] == (int)i,
                  "square modifie des éléments après la fin de l’intervalle"
                  " demandé");
  }

  check_log("Appel à free_buffer");
  free_buffer(data);
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_multiple_squares) {
  check_log("Appel à create buffer");
  int *data = create_buffer(1000*sizeof(int));
  ck_assert_msg(data != NULL && data != (void*)-1,
                "create_buffer ne devrait pas renvoyer un pointeur invalide.");

  srand(1252);
  for (size_t i = 0; i < 1000; i++) {
    check_log("Écriture à l’indice %zu d’un tableau de taille %d",
              i, 100);
    data[i] = rand() % 1000;
  }

  pid_t children[10];
  for (size_t i = 0; i < 10; i++) {
    check_log("Création d’un processus enfant");
    pid_t pid = fork();
    if (pid == 0) {
      square(data, i*100, 100);
      exit(0);
    }

    ck_assert(pid != -1);
    children[i] = pid;
  }

  check_log("Attente de la fin de l’exécution des processus enfants.");
  for (size_t i = 0; i < 10; i++) {
    int status;
    waitpid(children[i], &status, 0);
    ck_assert_msg(WIFEXITED(status), "Un des processus enfants ne s’est pas"
                  " terminé normalement");
  }

  srand(1252);
  for (size_t i = 0; i < 1000; i++) {
    check_log("Lecture de l’élément à l’indice %zu d’un tableau "
              "de taille 1000", i);
    int actual = data[i];
    int expected = rand() % 1000;
    expected *= expected;

    ck_assert_msg(actual == expected,
                  "La valeur à l’indice %zu est %d mais aurait dû être %d",
                  i, actual, expected);
  }

  check_log("Appel à free_buffer");
  free_buffer(data);
} NO_EARLY_EXIT_END_TEST

Suite *shm_suite(void) {
  Suite *s = suite_create("Mémoire partagée");

  TCase *tc_core = tcase_create("Core");
  tcase_add_logging_test(tc_core, test_create);
  tcase_add_logging_test(tc_core, test_square);
  tcase_add_logging_test(tc_core, test_multiple_squares);
  suite_add_tcase(s, tc_core);

  return s;
}

int main(int argc, char **argv) {
  if (check_early_exit_setup() < 0) {
    fprintf(stderr, "Failed to setup early exit testing.\n");
    return EXIT_FAILURE;
  }

  srand(0);

  Suite *s = shm_suite();
  SRunner *sr = srunner_create(s);

  check_init_logs();
  srunner_run_all(sr, CK_NORMAL);
  int number_failed = srunner_ntests_failed(sr);
  number_failed = check_early_exit(number_failed);
  check_print_logs();
  srunner_free(sr);

  return number_failed == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}
