=====================
 La mémoire partagée
=====================

:author: Kilian Verhetsel
:environment: systinfo
:name: shared_memory
:order: 7
:limit-time: 10
:limit-memory: 128
:limit-output: 5210

Le système d’exploitation sépare les différents programmes en cours d’exécution
en des processus indépendant. Néanmoins, il est parfois intéressant de permettre
à deux processus de communiquer. Un des mécanismes à votre disposition pour cela
est la création d’un segment de mémoire partagée.

Dans cet exercice, un processus va répartir une tâche entre plusieurs processus
enfants qui s’exécuteront simultanément. Les enfants doivent effectuer la tâche
qui leur est attribuée et stocker le résultat dans un segment de mémoire
partagée de manière à ce que le père puisse ensuite y accéder.

Création d’un segment de mémoire partagée
=========================================

.. question:: shared_memory
   :type: code
   :language: c

   Écrivez le contenu d’un fichier qui définit les deux fonctions suivantes (ainsi
   que tout autre définition dont vous avez besoin) :

   1. ``void *create_buffer(size_t size)`` qui alloue un segment de mémoire
      partagée dont la taille en bytes vaut au moins ``size`` ;
   2. ``void free_buffer(void *buffer)`` qui le libère.

   Le contenu du segment de mémoire doit être initialisé à 0.

Manipulation de la mémoire
==========================

.. question:: square
   :type: code
   :language: c

   Chaque enfant va exécuter la fonction ``void square(int *buffer, size_t start,
   size_t length)``, qui doit parcourir les éléments allant de ``buffer[start]``
   jusque ``buffer[start + length]`` (exclu) et remplacer chacun d’entre eux par
   son carré.

   Écrivez le corps de cette fonction ``square``.
