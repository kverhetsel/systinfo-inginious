===========
 Les types
===========

:author: Kilian Verhetsel
:environment: systinfo
:name: types
:order: 4
:limit-time: 10
:limit-memory: 128
:limit-output: 5210

La type de la plupart des variables en C est facile à déterminer. Néanmoins, le
C contient aussi des types qui ne diffèrent que de façons subtiles, comme les
types suivants, tous distincts :

- un tableau d’entiers ;
- un pointeur sur un entier ;
- un pointeur sur un entier constant ;
- un pointeur constant sur un entier.

Cet exercice sert à tester votre connaissance de la syntaxe utilisée pour
déclarer des valeur utilisant les différents types en C.

Un entier
=========

.. question:: q1
   :type: code-single-line
   :language: c

   Déclarez la variable ``x`` de type ``int``.

Un tableau d’entiers
====================

.. question:: q2
   :type: code-single-line
   :language: c

   Déclarez la variable ``x`` comme un tableau de 10 ``int``.

Taille d’un tableau
===================

.. question:: q3
   :type: code-single-line
   :language: c

   En supposant que ``sizeof(int)`` vaut 2 et que la taille de tout pointeur
   vaut 8, que vaut ``sizeof(x)`` dans la question précédente ?

Un pointeur
===========

.. question:: q4
   :type: code-single-line
   :language: c

   Déclarez la variable ``x`` comme un pointeur sur un ``int``.

Taille d’un pointeur
====================

.. question:: q5
   :type: code-single-line
   :language: c

   En supposant que ``sizeof(int)`` vaut 2 et que la taille de tout pointeur
   vaut 8, que vaut ``sizeof(x)`` dans la question précédente ?

Taile d’un tableau en tant qu’argument
======================================

.. question:: q6
   :type: code-single-line
   :language: c

   Voici le prototype d’une fonction standard::

     int pipe(int filedes[2]);

   En supposant que ``sizeof(int)`` vaut 2 et que la taille de tout pointeur
   vaut 8, que vaut ``sizeof(filedes)`` à l’intérieur du corps d’une fonction
   avec un tel prototype?

Un tableau d’entiers à 2 dimensions
===================================

.. question:: q7
   :type: code-single-line
   :language: c

   Déclarez la variable ``x`` comme un tableau 2D composé de ``int`` avec 5
   colonnes et 8 lignes.

Éléments constants
==================

.. question:: q8
   :type: code-single-line
   :language: c

   Déclarez la variable ``x`` comme un pointeur sur un ``int`` en faisant en
   sorte que l’instruction suivant soit illégale::

     *x = 4;

   Tout en autorisant cette séquence d’instructions::

     int i = 0;
     x = &i;

Pointeur constant
==================

.. question:: q9
   :type: code-single-line
   :language: c

   Déclarez la variable ``x`` comme un pointeur sur un ``int`` en faisant en
   sorte que la séquence d’instructions suivante soit illégale::

     int i = 0;
     x = &i;

   Tout en autorisant cette instruction-ci::

      *x = 0;

Pointeur et éléments constants
==============================

.. question:: q10
   :type: code-single-line
   :language: c

   Déclarez la variable ``x`` comme un pointeur sur un ``int`` de manière à ce
   que cette séquence d’instructions soit illégale::

     int i = 0;
     x = &i;

   De même que cette instruction::

     *x = 0;

Partage entre plusieurs fichiers
================================

.. question:: q11
   :type: code-single-line
   :language: c

   Supposez que la ligne suivante se trouve dans un autre fichier, dans l’espace
   global::

     int x;

   Écrivez la déclaration qui vous permet d’utiliser cette même variable ``x``
   depuis le fichier courant (typiquement, cette ligne se trouvera dans un
   *header*).

Variable locale à un fichier
============================

.. question:: q12
   :type: code-single-line
   :language: c

   Déclarez la variable ``x`` comme un ``int`` accessible uniquement depuis le
   fichier actuel (ou plutôt, l’unité de compilation actuelle).
