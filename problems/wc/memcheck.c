#include <sys/types.h>

ssize_t count_bytes(const char *filename);
ssize_t count_lines(const char *filename);

#include "utils.h"

int main(int argc, char **argv) {
  for (size_t i = 0; i < 10; i++) {
    write_random_file("bat", NULL, NULL);
    count_bytes("bat");
    count_lines("bat");
  }

  count_bytes("batman");
  count_lines("superman");

  return 0;
}
