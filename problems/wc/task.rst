===================================
 Compter les éléments d’un fichier
===================================

:author: Kilian Verhetsel
:environment: systinfo
:name: wc
:order: 8
:limit-time: 10
:limit-memory: 128
:limit-output: 5210

La commande ``wc`` permet de compter le nombre de bytes, caractères, mots ou
lignes qui se trouvent dans un fichier. Dans cet exercice, vous allez
implémenter une partie de cette fonctionnalité.

Compter les bytes
=================

.. question:: count_bytes
   :type: code
   :language: c

   Écrivez le corps fonction ``ssize_t count_bytes(const char *filename)`` qui
   renvoie le nombre de bytes se trouvant dans le fichier ``filename``, ou ``-1``
   en cas d’erreur.

Compter les lignes
==================

.. question:: count_lines
   :type: code
   :language: c

   Écrivez le corps fonction ``ssize_t count_lines(const char *filename)`` qui
   renvoie le nombre de caractères de retour à la ligne se trouvant dans le
   fichier ``filename``, ou ``-1`` en cas d’erreur.
