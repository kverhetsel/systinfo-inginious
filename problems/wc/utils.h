#include <stdio.h>
#include <stdlib.h>
#include <check.h>

static
void write_random_file(const char *filename,
                       size_t *out_size, size_t *out_line_count) {
  size_t size = rand() % 4096;
  size_t line_count = 0;
  unsigned char data[size];
  for (size_t i = 0; i < size; i++) {
    unsigned char c = rand() % 256;
    if (c == '\n') line_count++;
    data[i] = c;
  }

  FILE *f = fopen(filename, "w");
  fwrite(data, sizeof(data), 1, f);
  fclose(f);

  if (out_size) *out_size = size;
  if (out_line_count) * out_line_count = line_count;
}
