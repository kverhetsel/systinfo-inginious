#include <check_helpers.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

ssize_t count_bytes(const char *filename);
ssize_t count_lines(const char *filename);

#include "utils.h"

NO_EARLY_EXIT_START_TEST(test_count_bytes) {
  for (size_t i = 0; i < 10; i++) {
    size_t size;
    write_random_file("foo", &size, NULL);
    check_log("Appel à count_bytes");
    ssize_t counted = count_bytes("foo");
    ck_assert_msg(counted >= 0,
                  "count_bytes renvoie une erreur lors d’un appel valide.");
    ck_assert_msg((size_t)counted == size,
                  "La taille du fichier est %zu, mais count_bytes renvoie %ld",
                  size, counted);
  }
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_count_bytes_no_file) {
  for (size_t i = 0; i < 10; i++) {
    check_log("Appel à count_bytes avec un fichier non-existant");
    ssize_t counted = count_bytes("dont_create_this");
    ck_assert_msg(counted < 0,
                  "count_bytes ne renvoie pas d’erreur alors que le fichier "
                  "d’entrée n’existe pas.");
  }
} NO_EARLY_EXIT_END_TEST


NO_EARLY_EXIT_START_TEST(test_count_lines) {
  for (size_t i = 0; i < 10; i++) {
    size_t line_count;
    write_random_file("foo", NULL, &line_count);
    check_log("Appel à count_lines");
    ssize_t counted = count_lines("foo");
    ck_assert_msg(counted >= 0,
                  "count_lines renvoie une erreur lors d’un appel valide.");
    ck_assert_msg((size_t)counted == line_count,
                  "Le fichier contient '\\n' %zu fois, "
                  "mais count_lines renvoie %ld", line_count, counted);
  }
} NO_EARLY_EXIT_END_TEST

NO_EARLY_EXIT_START_TEST(test_count_lines_no_file) {
  for (size_t i = 0; i < 10; i++) {
    check_log("Appel à count_lines avec un fichier non-existant");
    ssize_t counted = count_lines("kaboum");
    ck_assert_msg(counted < 0,
                  "count_lines ne renvoie pas d’erreur alors que le fichier "
                  "d’entrée n’existe pas.");
  }
} NO_EARLY_EXIT_END_TEST


Suite *wc_suite(void) {
  Suite *s = suite_create("wc");

  TCase *tc_bytes = tcase_create("count_bytes");
  tcase_add_logging_test(tc_bytes, test_count_bytes);
  tcase_add_logging_test(tc_bytes, test_count_bytes_no_file);
  suite_add_tcase(s, tc_bytes);

  TCase *tc_lines = tcase_create("count_lines");
  tcase_add_logging_test(tc_lines, test_count_lines);
  tcase_add_logging_test(tc_lines, test_count_lines_no_file);
  suite_add_tcase(s, tc_lines);

  return s;
}

int main(int argc, char **argv) {
  if (check_early_exit_setup() < 0) {
    fprintf(stderr, "Failed to setup early exit testing.\n");
    return EXIT_FAILURE;
  }

  srand(0);

  Suite *s = wc_suite();
  SRunner *sr = srunner_create(s);

  check_init_logs();
  srunner_run_all(sr, CK_NORMAL);
  int number_failed = srunner_ntests_failed(sr);
  number_failed = check_early_exit(number_failed);
  check_print_logs();
  srunner_free(sr);

  return number_failed == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}
