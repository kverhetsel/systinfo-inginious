check-c-program
---------------

``check-c-program`` is the main command you can use to compile, link and run
tests against user code. Its basic usage is as follows:

    check-c-program -u user_file1.c,user_file2.c -t test1.c,test2.c

This will compile a program, link it against check and all the libraries that
are part of this project, and run it ``user_file.1.c``, ``user_file2.c``,
``test1.c`` and ``test2.c``. If the program exits with a non-zero status, its
output will be shown to the user.

You can also run a program in Valgrind to check for memory mishandling using the
``-m`` option:

    check-c-program -u user_file1.c,user_file2.c -m memory_program.c

At least one of ``-m`` and ``-t`` needs to be passed to the command. If both are
specified, the tests will be run first, followed by the memory testing.

Additional parameters can be passed to the various programs that this command
calls using environment variables:

  1. The C Compiler through ``$CFLAGS``
  2. The linker through ``$LDFLAGS``
  3. The ``feedback`` command through ``$FEEDBACK_FLAGS`` (typically to select a
     problem using ``-i problem_id``)
