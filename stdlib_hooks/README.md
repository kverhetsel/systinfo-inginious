stdlib_hooks
============

This is a C library that allows the user to alter the behavior of functions from
the standard library.

Compiling against stdlib_hooks
------------------------------

``stdlib_hooks`` is made up of two shared libraries :

   1. ``libstdlib_hooks``: An API to control which functions should be replaced
   2. ``libstdlib_hooks-preload``: To actually redefine functions.

You should link against the first one and run your program with
``LD_PRELOAD=/usr/lib/libstdlb_hooks-preload.so``. Also note that the
``check-c-program`` shell script takes care of doing that for you.

Memory allocation
-----------------

The main use of this is to temporary force memory allocation functions to
fail. You can do this using ``hook_alloc_force_fail()`` and
``hook_alloc_stop_fail()``:

    hook_alloc_force_fail();
    // malloc, realloc, calloc, aligned_alloc, etc. do not work
    hook_alloc_stop_fail();

It is also possible to replace ``malloc`` and other memory allocation functions
with your own:

    hook_current_malloc = my_malloc_replacement;
    // malloc now calls my_malloc_replacement
    hook_current_malloc = NULL;

Within the definition of ``hook_current_malloc``, it may be needed to call the
original ``malloc`` function. This is done by calling ``hook_real_malloc``.

If ``hook_current_malloc`` is set and ``hook_alloc_force_fail()`` has been
called, calls to ``malloc`` will return ``NULL`` without calling
``hook_current_malloc``.

If you don't want the user to be able to work around this (most of the time, you
want to see what happens if malloc fails and working around wouldn't help; this
is only useful for exercises which should be solved without dynamic memory
allocations), you will need to set the following function pointers to NULL:

  1. ``hook_real_malloc``
  2. ``hook_real_realloc``
  3. ``hook_real_aligned_alloc``
  4. ``hook_real_shmat``
  5. ``hook_real_mmap``

(You need to define ``_GNU_SOURCE`` for aligned_alloc to be available.)

If you need to use them, save the functions in a variable with local or static
storage (so it cannot be accessed from the user's compilation unit). If and when
you want to restore them, you will also need to call ``hook_alloc_stop_fail``.

Defining New Hooks
------------------

If you want to allow the library to define hooks on a function with the
prototype ``int foo(int a, size_t b)``, here are the steps you need to follow:

1. Including the header where the real function is defined in
   ``include/stdlib_hooks.h`` if need be.
2. Adding ``HookDeclare(foo)`` in ``include/stdlib_hooks.h``
3. Adding ``HookDef(foo)`` in ``src/defs.c``
4. Adding ``HookImpl(foo, int, list(int a, size_t b), list(a, b))`` in
   ``src/impl.c``. For void functions, use ``HookImplVoid`` instead.

Again, if you want to make sure students can't use the real function when you
redefine it, you need to set ``hook_real_foo`` to ``NULL`` (and set it back to
its original value when needed).

Use With Valgrind
-----------------

Internally, this library uses ``dlsym`` to retrieve pointers to the real
function. This causes Valgrind to sometimes incorrectly detect memory
leaks. There is a suppression file that you can use to fix the issue:

    valgrind --suppresions=/usr/share/stdlib_hooks/stdlib_hooks.supp ...
