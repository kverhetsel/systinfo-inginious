#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1
#endif

#include "stdlib_hooks.h"
#include <dlfcn.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

/*
 * dlsym may need to allocate memory before being able to return a pointer to
 * the really memory allocation functions. We make it so memory is allocated in
 * this buffer until we retrieve all memory allocation functions.
 *
 * This means we need to redefine free to be a no-op for pointers within this
 * buffer (we never reclaim the memory used here).
 */
static char *preallocated_buffer[4096] = {0};
static size_t buffer_i = 0;

extern int hook_should_fail;
static void *(*hook_real_calloc)(size_t, size_t) = NULL;
static void *(*hook_real_libc_calloc)(size_t, size_t) = NULL;

static void (*hook_real_free)(void*) = NULL;
static void *(*hook_real_libc_malloc)(size_t) = NULL;
static void *(*hook_real_libc_realloc)(void*, size_t) = NULL;

static void *(*hook_real_valloc)(size_t) = NULL;
static void *(*hook_real_libc_valloc)(size_t) = NULL;
static void *(*hook_real_pvalloc)(size_t) = NULL;
static void *(*hook_real_libc_pvalloc)(size_t) = NULL;

static int (*hook_real_posix_memalign)(void **, size_t, size_t) = NULL;
static void *(*hook_real_memalign)(size_t, size_t) = NULL;
static void *(*hook_real_libc_memalign)(size_t, size_t) = NULL;

static void *(*hook_real_mmap64)(void *addr, size_t len, int prot, int flags,
                                 int filedes, off64_t off) = NULL;

static int already_fetched = 0;

static
void get_alloc_functions(void) {
  if (already_fetched) {
    hook_should_fail = 1;
    return;
  }

  /*
   * If we set a hook on malloc, we don't want to execute it when dlsym
   * allocates memory or we may loop around forever. Therefore, we disable it
   * temporarily.
   */
  void *(*replacement)(size_t n) = hook_current_malloc;

  /* allocate this one as soon as possible */
  hook_real_calloc  = (void*(*)(size_t,size_t))dlsym(RTLD_NEXT, "calloc");
  hook_real_libc_calloc  = (void*(*)(size_t,size_t))
    dlsym(RTLD_NEXT, "__libc_calloc");
  hook_real_malloc  = (void*(*)(size_t))dlsym(RTLD_NEXT, "malloc");
  hook_real_libc_malloc  = (void*(*)(size_t))dlsym(RTLD_NEXT, "__libc_malloc");
  hook_real_valloc  = (void*(*)(size_t))dlsym(RTLD_NEXT, "valloc");
  hook_real_libc_valloc  = (void*(*)(size_t))dlsym(RTLD_NEXT, "__libc_valloc");
  hook_real_pvalloc  = (void*(*)(size_t))dlsym(RTLD_NEXT, "pvalloc");
  hook_real_libc_pvalloc  = (void*(*)(size_t))
    dlsym(RTLD_NEXT, "__libc_pvalloc");
  hook_real_realloc = (void*(*)(void*,size_t))dlsym(RTLD_NEXT, "realloc");
  hook_real_libc_realloc = (void*(*)(void*,size_t))
    dlsym(RTLD_NEXT, "__libc_realloc");
  hook_real_aligned_alloc =
    (void*(*)(size_t,size_t))dlsym(RTLD_NEXT, "aligned_alloc");
  hook_real_memalign = (void*(*)(size_t,size_t))dlsym(RTLD_NEXT, "memalign");
  hook_real_libc_memalign = (void*(*)(size_t,size_t))
    dlsym(RTLD_NEXT, "__libc_memalign");
  hook_real_posix_memalign = (int(*)(void**, size_t,size_t))
    dlsym(RTLD_NEXT, "posix_memalign");
  hook_real_shmat = (void*(*)(int,const void*, int))dlsym(RTLD_NEXT, "shmat");
  hook_real_mmap = (void*(*)(void*,size_t,int,int,int,off_t))
    dlsym(RTLD_NEXT, "mmap");
  hook_real_mmap64 = (void*(*)(void*,size_t,int,int,int,off64_t))
    dlsym(RTLD_NEXT, "mmap64");

  hook_current_malloc = replacement;

  already_fetched = 1;
}

void *malloc(size_t n) {
  if (!hook_real_malloc) get_alloc_functions();

  if (hook_should_fail) {
    errno = ENOMEM;
    return NULL;
  }
  else if (hook_current_malloc)
    return hook_current_malloc(n);
  else
    return hook_real_malloc(n);
}

void *__libc_malloc(size_t n) {
  if (!hook_real_malloc) get_alloc_functions();

  if (hook_should_fail) {
    errno = ENOMEM;
    return NULL;
  }
  else if (hook_current_malloc)
    return hook_current_malloc(n);
  else
    return hook_real_libc_malloc(n);
}

void *calloc(size_t n, size_t size) {
  if (hook_should_fail) {
    errno = ENOMEM;
    return NULL;
  }
  else if (hook_current_malloc) {
    /* Define calloc in terms of malloc to avoid having it fail */
    void *ptr = hook_current_malloc(n*size);
    if (ptr) memset(ptr, 0, n*size);
    return ptr;
  }
  else if (hook_real_calloc)
    return hook_real_calloc(n, size);
  else {
    size_t n_bytes = n * size;
    if (buffer_i + n_bytes > sizeof(preallocated_buffer)) {
      errno = ENOMEM;
      return NULL;
    }
    void *ptr = preallocated_buffer + buffer_i;
    buffer_i += n_bytes;
    return ptr;
  }
}

void *__libc_calloc(size_t n, size_t size) {
  if (!hook_real_malloc) get_alloc_functions();

  if (hook_should_fail) {
    errno = ENOMEM;
    return NULL;
  }
  else if (hook_current_malloc) {
    void *ptr = hook_current_malloc(n*size);
    if (ptr) memset(ptr, 0, n*size);
    return ptr;
  }
  else
    return hook_real_libc_calloc(n, size);
}

void *realloc(void *addr, size_t n) {
  if (!hook_real_realloc) get_alloc_functions();

  if (hook_should_fail) {
    errno = ENOMEM;
    return NULL;
  }
  else if (hook_current_realloc)
    return hook_current_realloc(addr, n);
  else
    return hook_real_realloc(addr, n);
}

void *__libc_realloc(void *addr, size_t n) {
  if (!hook_real_realloc) get_alloc_functions();

  if (hook_should_fail) {
    errno = ENOMEM;
    return NULL;
  }
  else if (hook_current_realloc)
    return hook_current_realloc(addr, n);
  else
    return hook_real_libc_realloc(addr, n);
}

void *valloc(size_t size) {
  if (!hook_real_malloc) get_alloc_functions();

  if (hook_should_fail) {
    errno = ENOMEM;
    return NULL;
  }
  else if (hook_current_aligned_alloc)
    return hook_current_aligned_alloc(sysconf(_SC_PAGESIZE), size);
  else
    return hook_real_valloc(size);
}

void *__libc_valloc(size_t size) {
  if (!hook_real_malloc) get_alloc_functions();

  if (hook_should_fail) {
    errno = ENOMEM;
    return NULL;
  }
  else if (hook_current_aligned_alloc)
    return hook_current_aligned_alloc(sysconf(_SC_PAGESIZE), size);
  else
    return hook_real_libc_valloc(size);
}

void *pvalloc(size_t size) {
  if (!hook_real_malloc) get_alloc_functions();

  size_t page_size = sysconf(_SC_PAGESIZE);
  if (size % page_size != 0)
    size += page_size - (size % page_size);

  if (hook_should_fail) {
    errno = ENOMEM;
    return NULL;
  }
  else if (hook_current_aligned_alloc)
    return hook_current_aligned_alloc(page_size, size);
  else
    return hook_real_pvalloc(size);
}

void *__libc_pvalloc(size_t size) {
  if (!hook_real_malloc) get_alloc_functions();

  size_t page_size = sysconf(_SC_PAGESIZE);
  if (size % page_size != 0)
    size += page_size - (size % page_size);

  if (hook_should_fail) {
    errno = ENOMEM;
    return NULL;
  }
  else if (hook_current_aligned_alloc)
    return hook_current_aligned_alloc(page_size, size);
  else
    return hook_real_libc_pvalloc(size);
}

void *aligned_alloc(size_t align, size_t n) {
  if (!hook_real_aligned_alloc) get_alloc_functions();

  if (hook_should_fail) {
    errno = ENOMEM;
    return NULL;
  }
  else if (hook_current_aligned_alloc)
    return hook_current_aligned_alloc(align, n);
  else
    return hook_real_aligned_alloc(align, n);
}

void *memalign(size_t align, size_t n) {
  if (!hook_real_aligned_alloc) get_alloc_functions();

  if (hook_should_fail) {
    errno = ENOMEM;
    return NULL;
  }
  else if (hook_current_aligned_alloc)
    return hook_current_aligned_alloc(align, n);
  else
    return hook_real_memalign(align, n);
}

void *__libc_memalign(size_t align, size_t n) {
  if (!hook_real_aligned_alloc) get_alloc_functions();

  if (hook_should_fail) {
    errno = ENOMEM;
    return NULL;
  }
  else if (hook_current_aligned_alloc)
    return hook_current_aligned_alloc(align, n);
  else
    return hook_real_libc_memalign(align, n);
}

int posix_memalign(void **memptr, size_t align, size_t n) {
  if (!hook_real_aligned_alloc) get_alloc_functions();

  if (hook_should_fail)
    return ENOMEM;
  else if (hook_current_aligned_alloc) {
    *memptr = hook_current_aligned_alloc(align, n);
    if (*memptr) return 0;
    else return ENOMEM;
  }
  else
    return hook_real_posix_memalign(memptr, align, n);
}

void *shmat(int shmid, const void *shmaddr, int shmflg) {
  if (!hook_real_shmat) get_alloc_functions();

  if (hook_should_fail)
    return (void*)-1;
  else if (hook_current_shmat)
    return hook_current_shmat(shmid, shmaddr, shmflg);
  else
    return hook_real_shmat(shmid, shmaddr, shmflg);
}

void *mmap(void *addr, size_t len, int prot, int flags,
           int filedes, off_t off) {
  if (!hook_real_mmap) get_alloc_functions();

  if (hook_should_fail) {
    errno = ENOMEM;
    return NULL;
  }
  else if (hook_current_mmap)
    return hook_current_mmap(addr, len, prot, flags, filedes, off);
  else
    return hook_real_mmap(addr, len, prot, flags, filedes, off);
}

void *mmap64(void *addr, size_t len, int prot, int flags,
           int filedes, off64_t off) {
  if (!hook_real_mmap) get_alloc_functions();

  if (hook_should_fail) {
    errno = ENOMEM;
    return NULL;
  }
  else
    return hook_real_mmap64(addr, len, prot, flags, filedes, off);
}

void free(void *ptr) {
  /*
   * Check this before fetching the function pointer to avoid being caught
   * in an infinite loop.
   */
  if ((void*)preallocated_buffer < ptr &&
      ptr < (void*)(preallocated_buffer+sizeof(preallocated_buffer)))
    return;

  if (!hook_real_free)
    hook_real_free = (void(*)(void*))dlsym(RTLD_NEXT, "free");
  hook_real_free(ptr);
}
