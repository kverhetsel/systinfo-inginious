#include "stdlib_hooks.h"

void *(*hook_current_malloc)(size_t n) = NULL;
void *(*hook_real_malloc)(size_t n)    = NULL;

void *(*hook_current_realloc)(void *addr, size_t n) = NULL;
void *(*hook_real_realloc)(void *addr, size_t n)    = NULL;

void *(*hook_current_aligned_alloc)(size_t align, size_t n) = NULL;
void *(*hook_real_aligned_alloc)(size_t align, size_t n)    = NULL;

void *(*hook_current_shmat)(int shmid, const void *shmaddr, int shmflg) = NULL;
void *(*hook_real_shmat)(int shmid, const void *shmaddr, int shmflg) = NULL;

void *(*hook_current_mmap)(void *addr, size_t len, int prot, int flags,
                           int filedes, off_t off) = NULL;
void *(*hook_real_mmap)(void *addr, size_t len, int prot, int flags,
                        int filedes, off_t off) = NULL;

int hook_should_fail = 0;

void hook_alloc_force_fail(void) {
  hook_should_fail = 1;
}

void hook_alloc_stop_fail(void) {
  hook_should_fail = 0;
}
