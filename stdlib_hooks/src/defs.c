#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1
#endif

#include "stdlib_hooks.h"

#define HookDef(function) \
  typeof(&function) hook_real_##function = NULL;             \
  typeof(&function) hook_current_##function = NULL;

HookDef(exit);
