#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1
#endif

#include "stdlib_hooks.h"
#include <dlfcn.h>

#define list(...) __VA_ARGS__

#define HookImpl(function, ret_type, param_list, param_names)    \
  ret_type function(param_list) {                                \
    static int fetched = 0;                                      \
    if (!fetched) {                                              \
      hook_real_##function = (typeof(&function))                 \
        dlsym(RTLD_NEXT, #function);                             \
      fetched = 1;                                               \
    }                                                            \
                                                                 \
    if (hook_current_##function)                                 \
      return hook_current_##function(param_names);               \
    else                                                         \
      return hook_real_##function(param_names);                  \
  }

#define HookImplVoid(function, param_list, param_names)          \
  void function(param_list) {                                    \
    static int fetched = 0;                                      \
    if (!fetched) {                                              \
      hook_real_##function = (typeof(&function))                 \
        dlsym(RTLD_NEXT, #function);                             \
      fetched = 1;                                               \
    }                                                            \
                                                                 \
    if (hook_current_##function)                                 \
      hook_current_##function(param_names);                      \
    else                                                         \
      hook_real_##function(param_names);                         \
  }

HookImplVoid(exit, list(int status), list(status))
