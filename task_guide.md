Writing Tasks
=============

This is a guide on how to add new tasks to this project, and what guidelines to
follow when modifying existing ones. To create a new task, a new sub-directory
should be created within the ``problems`` directory. In that directory, there
should be a ``task.rst`` file which describes the task. Here's what it may look
like (see INGInious' documentation for more detail):

    ============================================
     A Title That Will Be Shown To The Students
    ============================================

    :author: John Doe
    :environment: systinfo
    :name: problem_id_to_use_in_your_code
    :order: 98
    :limit-time: 10
    :limit-memory: 64
    :limit-output: 5210

    A general description of the task to be solved.

    The Name Of A Question
    ======================

    .. question:: id_to_use_in_your_code
       :type: code
       :language: c

       A description of that question (e.g. a single function they need to
       write).

    The Name Of Another Question
    ============================

    .. question:: another_id
       :type: code
       :language: c

       Another description.

Notice the ``:environment:`` parameter is set to ``systinfo``. This is needed
to use the tools written for this project. The ``:memory:`` parameter (in MB)
also needs to be high enough for the program to be able to run using Valgrind.

The other file that is always required is a ``run`` file. It will be executed
whenever the user submits their solution (and therefore, needs the execution bit
to be set). Most often, it will be a shell script that looks something like
this:

    #!/bin/bash

    getinput question_id > file1.h
    getinput question_id2 > file2.h

    check-c-program -u user_code.c -t your_code.c

Where ``user_code.c`` looks like this:

    // Usually, include whatever they may need
    #include <stdio.h>
    #include <stdlib.h>

    #include "file1.h"

    #include "file2.h"

The reason we use the C preprocessor to include user code, as opposed to
INGInious' templating mechanism, is so that the line numbers in error messages
from the various tools make sense to the student who submitted the code (care
should also be taken in picking a name for each file that isn't ambiguous).

``check-c-program`` takes care of compiling your program and printing error
messages if needed. It exits with a non-zero status if the student's input
failed. You can pass extra parameters to the C compiler and linker using
(respectively) the ``CFLAGS`` and ``LDFLAGS`` environment variables.

Writing Tests
-------------

Usually, your tests will be written using ``check``, a C unit testing
framework. This is what your main function should look like:

    #include <check_helpers.h>
    #include <stdio.h> // for fprintf
    #include <stdlib.h> // for EXIT_SUCCESS/EXIT_FAILURE

    int main(int argc, char **argv) {
      if (check_early_exit_setup() < 0) {
        fprintf(stderr, "Failed to setup early exit testing.\n");
        return EXIT_FAILURE;
      }

      Suite *s = your_problem_suite();
      SRunner *sr = srunner_create(s);

      check_init_logs();
      srunner_run_all(sr, CK_NORMAL);
      int number_failed = srunner_ntests_failed(sr);
      number_failed = check_early_exit(number_failed);
      check_print_logs();
      srunner_free(sr);

      return number_failed == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
    }

This takes care of:

- making it so answers like ``exit(0)`` don't pass your tests;
- allowing you to print useful messages if a test crashed.

The ``your_problem_suite`` function looks like this:

    Suite *your_problem_suite(void) {
      Suite *s = suite_create("Descripton");

      TCase *tc_a = tcase_create("Some group of tests");
      tcase_add_logging_test(tc_a, test_a);
      suite_add_tcase(s, tc_a);

      TCase *tc_b = tcase_create("Some other group of tests");
      tcase_add_logging_test(tc_b, test_b);
      tcase_add_logging_test(tc_b, test_c);
      tcase_add_logging_test(tc_b, test_d);
      suite_add_tcase(s, tc_b);

      return s;
    }

Notice the use of ``tcase_add_logging_test`` (instead of Check's
``tcase_add_test``). This is so because the ``check_helpers`` library needs to
know how many tests are going to use the ``check_log`` function (more on that
later). If you need to use some of Check's features not accessible by
``tcase_add_test``, use the ``tcase_add_logging_test_f`` function, which takes
the same parameters as ``_tcase_add_test``.

Each individual test function is going to look like this:

    NO_EARLY_EXIT_START_TEST(test_name) {
      /* your code here */
    } NO_EARLY_EXIT_END_TEST

Within test code, you will need to write assertions about the user's code. Most
of the time, this is done using ``ck_assert_msg`` (because it allows you to
write a useful message to the user, who should be able to understand the failure
without having to look at the test's code):

    ck_assert_msg(some_condition,
                  "Error message printed when some_condition is zero. "
                  "You can use %s here", "printf-style formatting");

The ``check_log`` function should be used within tests before any line
susceptible of causing a crash. This includes:

- dereferencing or freeing a pointer returned by the student's code;
- dereferencing or freeing a pointer that you created but also passed to the
 student's code;
- calling a function written by the student.

``check_log`` is used like ``printf``. If the code crashes, the last message
(and only that one) will be displayed to the user.

    check_log("Writing to index %zu", i);
    array[i] = 3;

Using Valgrind
--------------

The easiest way to ensure that the student's code doesn't corrupt or leak
memory, even though it passes all the tests, is to use Valgrind's ``memcheck``
tool.

If you have a ``memcheck.c`` file that contains a C program that uses the
student's code in a way that should not cause any memory leak for a correct
implementation, you can do this by change the call to ``check-c-program`` like
so:

    check-c-program -u user_code.c -t your_code.c -m memcheck.c

Making malloc Fail
------------------

You may want to make sure that the user's code works even if ``malloc``
fails. The ``stdlib_hooks`` library provides a way of doing that:

    #include <stdlib_hooks.h>

    hook_alloc_force_fail();
    /* malloc doen't work here */
    hook_alloc_stop_fail();

Sometimes though, your goal is not to test what happens when ``malloc`` fails,
but to forbid the student from using it. You then need to make sure they don't
get access to pointers to the actual memory allocation functions. Instead of
using ``hook_alloc_force_fail`` and ``hook_alloc_stop_fail`` you need to keep
track of function pointers yourself:

    static void *(*real_malloc)(size_t n) = NULL;
    static void *(*real_realloc)(void *addr, size_t n) = NULL;
    static void *(*real_aligned_alloc)(size_t align, size_t n) = NULL;
    static void *(*real_shmat)(int shmid, const void *shmaddr,
                               int shmflg) = NULL;
    static void *(*real_mmap)(void *addr, size_t len, int prot, int flags,
                              int filedes, off_t off) = NULL;

    void disable_malloc(void) {
      real_malloc        = hook_real_malloc;
      real_realloc       = hook_real_realloc;
      real_aligned_alloc = hook_real_aligned_alloc;
      real_shmat         = hook_real_shmat;
      real_mmap          = hook_real_mmap;

      hook_real_malloc        = NULL;
      hook_real_realloc       = NULL;
      hook_real_aligned_alloc = NULL;
      hook_real_shmat         = NULL;
      hook_real_mmap          = NULL;
    }

    void enable_malloc(void) {
      hook_real_malloc        = real_malloc;
      hook_real_realloc       = real_realloc;
      hook_real_aligned_alloc = real_aligned_alloc;
      hook_real_shmat         = real_shmat;
      hook_real_mmap          = real_mmap;

      hook_alloc_stop_fail();
    }

In this case, none of the functions run by the user should be run while
``hook_real_malloc`` (or any of the others) is non-null.
